---
title: "Archives"
---

{{<map-info
	img-thumb="ge_archives_small.jpg"
	img-full="ge_archives.jpg"
	mapname="ge_archives"
	author="Luchador"
	first-release="Beta 1"
	location="St. Petersburg, Russia"
	playercount="10-16"
>}}

Archives is a level for GoldenEye: Source. It takes place in a GRU (Russian Military Intelligence) archive. Archives was first done by InvertedShadow, but then completely remade by Luchador for Beta 4.0.

## History

The first version of Archives, released for Beta 1, only contained the GE64 multiplayer portion plus the large room seen in the original game's mission select photo. For this reason it was named “Archives Frontzone.” In Beta 3 InvertedShadow extended the map to include the entire GE 64 single player level and the “Frontzone” tag was dropped. Luchador completely redid the map from scratch for Beta 4, fixing layout problems and taking much visual inspiration from the film scenes. 
