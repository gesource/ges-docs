---
title: "Silo"
---

{{<map-info
	img-thumb="ge_silo_small.jpg"
	img-full="ge_silo.jpg"
	mapname="ge_silo"
	author="Luchador"
	first-release="Beta 4.0"
	location="Arkhangelsk, USSR"
	playercount="8-12"
>}}

Silo is a map for GoldenEye: Source.

## History

Silo was in development by Sporkfire since Beta 2 and was initially released with Beta 4. The map underwent a complete overhaul by Luchador for 4.1 with new textures, models, a refined and larger layout and a cleaner overall look that is more reminiscent of the original game. As a result, Silo set the standard for quality of future map releases and map updates. 
