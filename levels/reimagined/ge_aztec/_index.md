---
title: "Aztec"
---

{{<map-info
	img-thumb="ge_aztec_small.jpg"
	img-full="ge_aztec.jpg"
	mapname="ge_aztec"
	author="Luchador"
	first-release="Beta 4.0"
	location="San Juan Teotihuacán, Mexico"
	playercount="8-16"
>}}

Aztec is a level for GoldenEye: Source.

## History

InvertedShadow created a version of the map for Beta 4.0. A new version was made by Luchador for 5.0.
