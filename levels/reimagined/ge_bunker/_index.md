---
title: "Bunker"
---

{{<map-info
	img-thumb="ge_bunker_small.jpg"
	img-full="ge_bunker.jpg"
	mapname="ge_bunker"
	author="Luchador"
	first-release="5.0"
	location="Severnaya, Russia"
	playercount="6-16"
>}}

Bunker is a level made for GoldenEye: Source.

## History

Bunker was originally a map by Semedi using mostly default Half-Life 2 assets. After Semedi's departure from the development team to work at EA, the map was left untouched for several months until Semedi posted his .vmf source for the map on the development forums. SharpSh00tah then picked up the map and cleaned up much of the base geometry to make the overall compiled product cleaner. The map was then passed onto The SSL for detailing, but the map remained unfinished. Then Fourtecks remodeled the main control room.

After that, Luchador scrapped everything outside of the control room and restarted from scratch. By the end of 2015, the map had finally reached substantial completion and final touches were done prior to the 5.0 release. 
