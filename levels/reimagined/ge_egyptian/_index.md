---
title: "Egyptian"
---

{{<map-info
	img-thumb="ge_egyptian_small.jpg"
	img-full="ge_egyptian.jpg"
	mapname="ge_egyptian"
	author="EnzoMatrix, Torn"
	first-release="Alpha 1.0"
	location="Ezbet Rushdi el-Saghira, Egypt"
	playercount="4-12"
>}}

Originally inspired by the Egyptian ruins featured in *[The Spy Who Loved Me](http://en.wikipedia.org/wiki/The_Spy_Who_Loved_Me_%28film%29)*, Egyptian was both a single player and multiplayer map in GoldenEye 007, but allowed only 2 players for multiplayer. Most of the original features, like the secret doors and rooms have been kept the same, enhanced by more in-depth lighting, diverse landscapes, texture variety, and numerous small details.

## History

Started by GE:S co-founder Enzo as one of the first maps in the game, the level is based off the original from the Nintendo 64. With new features and areas added in Beta 1.0 it kept its feel but was capable of holding many more players.

Egyptian was the first level to be playable but also had the most difficulties. During its production many complications were halting its progress. The first was the pillars. They took nearly eight months to be added because all members of the team were still learning how to properly compile props.


### 5.0

After some experimentation by Fourtecks, the level was eventually taken on by Torn for the 5.0 release. Using most of Enzo's layout, Torn created a completely new texture set and look for Egyptian that kept the spirit of the original while also incorporating new themes and ideas.
Film

## Film

In *The Spy Who Loved Me*, Bond and KGB agent Anya Amasova, working together to secure a microfilm of plans for a new submarine tracking system, stow away in Jaws' van in order to find out where he is taking the microfilm and retrieve it. Jaws drives to the ruins of an ancient Egyptian temple where he realises he is being persued and disappears amongst the maze of pillars before ambushing them.

This scene was filmed at the Karnak Temple Complex, Luxor, Egypt.


## Trivia

In GoldenEye 007, the player must complete all 19 missions on 00 Agent difficulty to unlock Egyptian. 
