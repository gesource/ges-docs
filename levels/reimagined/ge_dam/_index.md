---
title: "Dam"
---

{{<map-info
	img-thumb="ge_dam_small.jpg"
	img-full="ge_dam.jpg"
	mapname="ge_dam"
	author="Torn"
	first-release="5.0"
	location="Byelomorye Dam,{{<br>}}Arkhangelsk, USSR"
	playercount="6-16"
>}}

Dam is a remake of the first level of the original game. It was made by Torn and was released with version 5.0. The layout differs significantly from the original layout to facilitate good gameplay, but the spirit of the map and all the iconic elements remain intact with updated visuals.
