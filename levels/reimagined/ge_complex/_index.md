---
title: "Complex"
---

{{<map-info
	img-thumb="ge_complex_small.jpg"
	img-full="ge_complex.jpg"
	mapname="ge_complex"
	author="Lo-Fi, Luchador"
	first-release="Beta 1.0"
	location="Unknown"
	playercount="4-10"
>}}

Complex was one of the most popular multiplayer maps in Goldeneye 007. This Source version breathes new life into an old favorite with bright lights, sharp textures, and a quasi-futuristic look. 

## History

Complex was upgraded for 5.0 by Luchador. Most of the textures were remade at double the resolution and brush-based pipes with shading issues were replaced with models. 
