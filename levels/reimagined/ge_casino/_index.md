---
title: "Casino"
---

{{<map-info
	img-thumb="ge_casino_small.jpg"
	img-full="ge_casino.jpg"
	mapname="ge_casino"
	author="Luchador"
	first-release="4.1"
	location="Podgorica, Montenegro"
	playercount="6-10"
>}}

Casino is a level made for GoldenEye: Source based off the casino from *Casino Royale* (2006).

## History

Development for this map started in April 2008 as cr_casino. The idea for the map was brought up by Rusky, who provided screens of the movie, and references from actual casinos. Frexx was a private beta tester for the map.

The map was first released in August of 2008 on The Nations War servers. Since then, others have put the map on their servers, and a download for the map used to be available on The Nations War website, but the website is now dead.

An updated and official version of Casino has been in the official release of GE:S since 4.1.

## References

[An interview with Rusky](http://forums.goldeneyesource.net/index.php/topic,2759.msg28688.html#msg28688)
