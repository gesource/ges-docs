---
title: "Runway"
---

{{<map-info
	img-thumb="ge_runway_small.jpg"
	img-full="ge_runway.jpg"
	mapname="ge_runway"
	author="Luchador"
	first-release="Beta 3.0"
	location="Arkhangelsk, USSR"
	playercount="9"
>}}

Runway is a map for GoldenEye: Source. Being a modernized level, it combines aspects of the GoldenEye 007 layout with the set design of the movie.

## History

Runway was a map originally developed by SharpSh00tah. His version combined elements of the GoldenEye 007 counterpart, the movie, and even some new areas that have yet to be explored. Sharpshootah used his creative impulse to expand on these areas in order to better suit the map's gameplay in a multiplayer environment by making it larger with more indoor environments to take cover in and to make it much more dynamic than the Nintendo 64 version which wasn't as suitable for multiplayer gameplay.

Looking out at his version of runway, there are a lot of the elements from the GoldenEye 007 level, but taken to the next step in terms of detail. You now could clearly see the facility in the back, and the addition of fog helps set the tone for the level. On the opposing sides of the runway, there were buildings; in this GE:S rendition you can go inside them, and they connect to various other places around the map. This runway mixes both outdoor and indoor elements and molds them into one.

The map was eventually taken over by CCSaint during the 4.2 release, with a focus mostly on gameplay. E-S helped with layout changes and Mangley assisted with visual updates. Many ideas were experimented with, like the addition of areas that would open up once a sufficient player count was reached, and an endless pit at the end of the runway.

For 5.0 the map was lagging behind in terms of visuals and was thus taken over by Luchador. He opted to rebuild the map from scratch and go in a different direction that sought to replicate the look of the facility in the movie. Some performance issues were encountered towards the end of development, but they were rectified before the 5.0 release. 
