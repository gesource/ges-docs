---
title: "Facility"
---

{{<map-info
	img-thumb="ge_facility_small.jpg"
	img-full="ge_facility.jpg"
	mapname="ge_facility"
	author="Fourtecks"
	first-release="Alpha 1.0"
	location="Arkhangelsk, USSR"
	playercount="4-12"
>}}

Facility is one of the most popular levels from GoldenEye 007. This re-envisioned Facility is true to the original, with a few gameplay changes. Subtle variations in texture and lighting, as well as numerous small details, give depth to this map, while the omnipresence of metal and concrete tie it together. 

## History

 Facility was originally being made by a previous GE:S Dev member that went by the alias Baddog. After having some troubles with the level, he gave it up, and Fourtecks took the reins and started from scratch. Right from the start, Facility was envisioned as the run-down Russian chemical facility in the movie, instead of the clean tiled wall version in the original game. The texturing of this level has changed a lot in favor of better quality as Fourtecks' texture creation improved. After some consideration some game texture elements from the N64 version made its way back into parts of the level, but with an upgraded twist to them. This change of heart is considered to bring back a bit of nostalgia, something that is very welcomed.

Originally all wall corners in the level were going to be rounded off in some sort of fashion. The effect was considered not to be the right look for the map and the walls have been squared out again, except for a slight few.

The Beta 1 version of Facility featured an underground passage that connected various sections of the map.

[![Screenshot of Facility Beta 1](facility_basement_beta1.jpg)](facility_basement_beta1.jpg)

## Film

In the film, Facility is a Soviet chemical weapons plant located at the foot of a dam in the remote mountains of [Arkangel](http://en.wikipedia.org/wiki/Arkhangelsk_Oblast). After bungee jumping off the dam and cutting open an access panel, Bond infiltrates the facility through the ventilation ducts above the bathrooms. He incapacitates a guard in the toilet below and proceeds out of the bathrooms, down the stairs, and into the adjacent storage room where he meets 006. They then lift an access grate out of the floor through which they proceed to the Backzone where their mission is to destroy the entire chemical weapon facility by placing explosives on the chemical bottling tanks.

## Trivia

* The red armor room has been given elevator music early in its life cycle to test out the new remix created by our very own Basstronix. Due to popular demand and love of the track, the music has kept its place in the room even though it's not an elevator.
* There is a message in a bathroom stall that states 'For a good time, sv_paintball 1', in reference to the server side command for paintball mode.
* In the GoldenEye movie, the labs by the washroom were actually used for food storage. The one at the bottom of the stairs for canned foods, and the one at the end of the hall was used as a freezer.
* The cafeteria seen in the back hallway was actually attached to the lab at the bottom of the stairs in the GoldenEye movie.

