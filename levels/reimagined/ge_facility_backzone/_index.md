---
title: "Facility Backzone"
---

{{<map-info
	img-thumb="ge_facility_backzone_small.jpg"
	img-full="ge_facility_backzone.jpg"
	mapname="ge_facility_backzone"
	author="Fourtecks"
	first-release="Alpha 1.0"
	location="Arkhangelsk, USSR"
	playercount="4-12"
>}}

Facility Backzone was not an official multiplayer map in GoldenEye 007. It was part of the single player Facility level. Thanks to Gameshark codes this map could be played complete with weapons, ammo, and multiple spawn points.

In GoldenEye: Source, Facility Backzone was heavily influenced by both the game and the movie and several entirely new areas were added by the our level designers to help with flow and balance.

Jeo, Fourtecks, and many more have contributed to this map.

## Trivia

* The red armor room has been given elevator music so it can be equal with its Frontzone counterpart. Due to popular demand and love of the track, the music has kept its place in the room even though it's not an elevator.
* The vent in the floor in the lab section is referencing a similar vent that 007 used to gain access to the Backzone of the Facility.
