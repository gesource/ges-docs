---
title: "Basement"
---

{{<map-info
	img-thumb="ge_basement_small.jpg"
	img-full="ge_basement.jpg"
	mapname="ge_basement"
	author="SSL, Luchador"
	first-release="Beta 4.0"
	location="London, England"
	playercount="4-12"
>}}

## History

### 1.0 to 4.1

The map originally began as a Beta 1 community map by SSL. Development carried on into Beta 3.x but was never released. Basement was then added to the official Beta 4.0 map list when The SSL became an official developer.

### 5.0

The map received a major overhaul by Luchador. Basement was redesigned as an old MI6 archive. A few layout adjustments were made to improve gameplay. Decade-old Half Life 2 models and textures were removed and replaced with new art assets. 