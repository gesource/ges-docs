---
title: "Depot"
---

{{<map-info
	img-thumb="ge_depot_small.jpg"
	img-full="ge_depot.jpg"
	mapname="ge_depot"
	author="Adrian"
	first-release="5.0"
	location="St. Petersburg, Russia"
	playercount="6-16"
>}}

Depot is a level for GoldenEye: Source.

The map was first seen in a trailer for beta 1, and seemed finished, but it was later revealed that only a few areas were complete. Level designer Adrian started the level anew in 2015 and released it for version 5.0.
