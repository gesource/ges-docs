---
title: "Caverns"
---

{{<map-info
	img-thumb="ge_caverns_small.jpg"
	img-full="ge_caverns.jpg"
	mapname="ge_caverns"
	author="Luchador"
	first-release="Beta 4.0"
	location="Cuba"
	playercount="6-16"
>}}

Caverns is a level made for GoldenEye: Source.

## History

Caverns was originally a map by SharpSh00tah released in Beta 4. Luchador completely rebuilt the map for the 5.0 update, using a new layout designed by Entropy-Soldier.
