---
title: "Cradle"
---

{{<map-info
	img-thumb="ge_cradle_small.jpg"
	img-full="ge_cradle.jpg"
	mapname="ge_cradle"
	author="Luchador"
	first-release="Beta 1.0"
	location="Cuba"
	playercount="6-12"
>}}

Cradle is the last level in GoldenEye 007. Suspended high above the earth, this open-air map is dominated by long-distance combat. 

## History

Cradle was originally a map by Fonfa released in Beta 1.0. The map received a large overhaul to visuals and layout by Luchador in 2016 for the 5.0 update. 

## Film

This scene was filmed at the [Arecibo Observatory, Puerto Rico](http://en.wikipedia.org/wiki/Arecibo_Observatory). The Observatory regrettably collapsed December 1st, 2020.
