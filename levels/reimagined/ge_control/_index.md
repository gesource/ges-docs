---
title: "Control"
---

{{<map-info
	img-thumb="ge_control_small.jpg"
	img-full="ge_control.jpg"
	mapname="ge_control"
	author="Fourtecks"
	first-release="Beta 1.0"
	location="Cuba"
	playercount="6-12"
>}}

Control was not a multiplayer map in Goldeneye 007, but this recreation shows that its cyclic design is well-suited to that purpose. Featuring rough, dirty caves lit with harsh floodlights and a bright, clean control room, this map blends two very different environments into a single dynamic game.  

## History

Control is the level that resides under the Cradle and is linked with Caverns between the two in terms of location. Design of this level began in early February 2005 by Fourtecks as a way to apply for a level design position on GoldenEye: Source. The map has been through many different changes for quality reasons.

The level was first designed as a slightly upgraded version from the original game, sporting more emphasis on cavernous-looking backrooms/halls. This was later scrapped entirely due to the odd looking architecture and bland look of the rock walls being everywhere. Let's face it, the level in the original game wasn't too good looking. The level is now a mix between the game and movie, and Fourtecks let personal freedom roam on areas not seen in the movie. The final design is to make the main control room blue and modern, the backrooms/halls concrete and industrial, and the caves area a dig site. 
