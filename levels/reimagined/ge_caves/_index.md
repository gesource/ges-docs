---
title: "Caves"
---

{{<map-info
	img-thumb="ge_caves_small.jpg"
	img-full="ge_caves.jpg"
	mapname="ge_caves"
	author="Luchador"
	first-release="Alpha 1.0"
	location="Unknown"
	playercount="4-12"
>}}

Caves is generally considered to be the worst multiplayer map in GoldenEye 007, partly due to its dark setting and confusing layout. The version of the map created for GoldenEye: Source is vastly improved over the original, with more diverse lighting and better level balance. Caves has also had an extension attached to one end to balance the gameplay, suit numerous gametypes, and handle the increased number of players. 

## History

Caves was viewed by many as the boogeyman; always leering over your shoulder, but not a map anybody of sound mind would engage. Lo-fi, a long-time fan and observer of the mod's progression, applied to the development team and offered to make the Caves map, much to everyone's surprise. Caves had been his favorite map from the original game, and coupled with grenade launchers, all hell would break loose, so he had a special attachment to the level. Mod founder Nickster figured the applicant must have a death wish, but decided that it was probably the team's best chance for somebody to actually complete this God-forsaken level. Lo-fi flexed some major displacement muscles and bashed out a playable version of the map from scratch with time left over to polish for initial Christmas launch of the mod.

The map, as a contribution to the game, has gained even more personal relevance to Lo-fi with the passing of the mod's founder. Nickster was a principal figure in offering strong encouragement to Lo-fi in his early days with the team and with the map's development, and as such, he views his contributions to the map as a standout of their friendship. His vision, and the work he put into it, can be felt even in the newest versions of the map.

Lo-Fi's version of the map held up for a long time, but after many years eventually fell behind the rest visually and no longer accommodated all elements of the game's ever evolving gameplay. Because of this, the team decided to take the lessons learned from it and incorporate them into an all new version of the map. This map was started by Mangley and completed by Luchador for the 5.0 release.
