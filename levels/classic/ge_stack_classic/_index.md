---
title: "Stack Classic"
---

{{<map-info
	img-thumb="ge_stack_classic_small.jpg"
	img-full="ge_stack_classic.jpg"
	mapname="ge_stack_classic"
	author="JcFerggy, Adrian, Mangley"
	first-release="Beta 3.0"
	location="Unknown"
	playercount="2-8"
>}}

Stack Classic is a level in GoldenEye: Source. This map is the upper part of [Library Classic](../ge_library_classic). 

## History

Originally worked on by Fourtecks, for 5.0 the basic geometry was rebuilt by JcFerggy who then handed it off to Adrian to finish everything else. After a final art pass by Mangley the map was ready for release. 
