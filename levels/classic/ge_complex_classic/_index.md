---
title: "Complex Classic"
---

{{<map-info
	img-thumb="ge_complex_classic_small.jpg"
	img-full="ge_complex_classic.jpg"
	mapname="ge_complex_classic"
	author="Lo-Fi"
	first-release="Beta 1.0"
	location="Unknown"
	playercount="2-10"
>}}

Complex Classic is a multiplayer level for GoldenEye: Source. The basis for this level is to try to get as close to the original Complex from GoldenEye 007 as possible. Complex was one of the most popular maps in GoldenEye 007, and this version made by Lo-Fi lives up to the classic. 
