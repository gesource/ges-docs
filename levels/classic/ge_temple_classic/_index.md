---
title: "Temple Classic"
---

{{<map-info
	img-thumb="ge_temple_classic_small.jpg"
	img-full="ge_temple_classic.jpg"
	mapname="ge_temple_classic"
	author="Luchador"
	first-release="Beta 1.0"
	location="Unknown"
	playercount="2-8"
>}}

Temple Classic is a level in GoldenEye: Source.
