---
title: "Archives Classic"
---

{{<map-info
	img-thumb="ge_archives_classic_small.jpg"
	img-full="ge_archives_classic.jpg"
	mapname="ge_archives_classic"
	author="Adrian"
	first-release="5.0"
	location="St. Petersburg, Russia"
	playercount="4-12"
>}}

Archives Classic is a level made for GoldenEye: Source. This level includes the entire area from the GE64 single player mission rather than restricting itself to just the multiplayer section. 

## History

Luchador started an Archives Classic in 2011. Many textures were made, but the level was never finished.

In 2014, Adrian joined the GE:S development team and got started on his first project, Archives Classic. He started by referencing the original game's geometry to make his own level as accurate as possible. Some of Luchador's textures were re-used while other textures were contributed by Adrian himself.

The initial version of Archives Classic was completed by the end of the year. It was released as part of a Community Map Pack in January 2015 alongside two other (unofficial) maps, Blockfort and Train. Its first official release as part of GE:S was with version 5.0 in August of 2016.

[![Image of Archives Classic with Developer Textures](dev-screenshots/4Gn8EvJ.jpg?width=32%25&classes=inline,left)](dev-screenshots/4Gn8EvJ.jpg)
[![Image of Archives Classic with Developer Textures](dev-screenshots/R9AF2Kf.jpg?width=32%25&classes=inline,center)](dev-screenshots/R9AF2Kf.jpg)
[![Image of Archives Classic with Developer Textures](dev-screenshots/zExAxxf.jpg?width=32%25&&classes=inline,right)](dev-screenshots/zExAxxf.jpg)

*Some early development screenshots of Archives Classic from August 2014.*
