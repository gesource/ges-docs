---
title: "Library Classic"
---

{{<map-info
	img-thumb="ge_library_classic_small.jpg"
	img-full="ge_library_classic.jpg"
	mapname="ge_library_classic"
	author="JCFerggy, Adrian, Mangley"
	first-release="Beta 1.0"
	location="Unknown"
	playercount="2-12"
>}}

Library Classic is a level in GoldenEye: Source. The level is a combination of [Stack Classic](../ge_stack_classic) and [Basement Classic](../ge_basement_classic).

Due to its dynamic layout, this map is suitable for a wide range of players:

* With 0-4 players, only the Basement is accessible.
* With 5-8 players, only the Stack is accessible.
* With 9+ players, the whole Library is accessible.

## History

Originally worked on by Fourtecks, for 5.0 the basic geometry was rebuilt by JcFerggy who then handed it off to Adrian to finish everything else. After a final art pass by Mangley the map was ready for release. 
