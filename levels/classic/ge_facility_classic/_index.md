---
title: "Facility Classic"
---

{{<map-info
	img-thumb="ge_facility_classic_small.jpg"
	img-full="ge_facility_classic.jpg"
	mapname="ge_facility_classic"
	author="Luchador"
	first-release="Beta 1.0"
	location="Arkangelsk, Soviet Union"
	playercount="2-8"
>}}

Facility Classic is the classic version of Facility.

## History

Semedi first made a version of Facility Classic for Beta 1.0. This level included both the frontzone and backzone. As a result, the level was quite linear and proved problematic for multiplayer gameplay. The map had other problems including incorrect scale and it used textures ripped directly from the GE64 ROM.

A second version was made by Luchador for 4.1. This version did away with the backzone and all the textures were remade from scratch. This version, however, still had incorrect scale.

The third and final version was also made by Luchador and released for 5.0. In 2014 Luchador learned how to extract the original level geometry from the GE64 ROM using the GoldenEye Setup Editor. Then he discovered how to convert from GoldenEye 64 units to Source units in order to give the level correct scaling. He converted the original geometry to a model then used it as a reference to make a new map with brushes that are within one Source unit of the GE64 geometry. 
