---
title: "Bunker Classic"
---

{{<map-info
	img-thumb="ge_bunker_classic_small.jpg"
	img-full="ge_bunker_classic.jpg"
	mapname="ge_bunker_classic"
	author="Mangley"
	first-release="4.1"
	location="Severnaya, Russia"
	playercount="2-10"
>}}

Bunker Classic started as a third party map by Mangley for Beta 4. After its release, the team decided to make it an official map. 

## History

Prior to code enhancements in 5.0, it was not uncommon to see floating glass respawn in open doorways. The glass would then be offset from their intended position until broken again.
