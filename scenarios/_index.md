---
title: "Scenarios"
weight: 100
---

In GoldenEye: Source there are several gameplay scenarios from the original GoldenEye 007, as well as some new ones that we created. Below you can find a general summary of each scenario. Most of the scenario names are inspired by James Bond movie titles and some scenarios are even based on the content of the movie such as Man With The Golden Gun.

---

Legend:

*cname*: The file name of the gameplay to be used in scripts or console commands (such as `ge_gameplay`)

*Teamplay*: Whether the gameplay has teams or is free-for-all

---

## Deathmatch

> cname: deathmatch \
> Teamplay: Toggleable

Deathmatch is exactly like what you expect it to be, gritty hardcore violence aimed at your enemy's head. The goal is to kill as many people you can during each round. There is the option of enabling a frag limit to end the match and switch maps once that limit is reached by any player or team. Teamplay is supported. 

## Dr. No Armor

> cname: drnoarmor \
> Teamplay: Toggleable

This gameplay is exactly like Deathmatch, except that all armor is disabled.

## Tournament Deathmatch

> cname: tournamentdm \
> Teamplay: Toggleable

Tournament Deathmatch is played out exactly like Team Deathmatch except that team spawns are used and a “warmup” time is available to be set. This gameplay scenario is designed for clan matches and LAN games primarily. Teamplay is forced on. 

## License to Kill

> cname: ltk \
> Teamplay: Toggleable

License To Kill is a game mode in which any hit with any weapon to any body part causes instant death regardless of health or armor. Teamplay is supported. While playing, kill based achievements have a 3-1 ratio due to the fast nature of gameplay. Radar is disabled by default. 

## You Only Live Twice

> cname: yolt \
> Teamplay: Toggleable

You Only Live Twice! Once you lose your second life you will be forced into observer mode and cannot respawn until the next round. The winner is the last player standing.

## Man With The Golden Gun

> cname: mwgg \
> Teamplay: Never

Man with the Golden Gun exemplifies pistol marksmanship and competitive gameplay. The goal is to grab the Golden Gun that is somewhere on the map and use it to vanquish your opponents. As soon as you grab the gun you are painted as a bright gold target on the radar and everyone will be trying to kill you. The Golden Gun does not have a magazine and must be reloaded after every shot, making it very important that you place your shots carefully, although one shot kills!

## Capture The Flag

> cname: capturetheflag \
> Teamplay: Always

Capture the Flag should feel familiar to anyone who has played it in other games. Players are divided into 2 teams, and each team must capture and return the enemy flag to their capture point.

If a flag carrier is killed, walking over the dropped flag will return it to spawn point immediately, unless another enemy team mate picks it up first. Points are distributed as 5 points for capturing the flag, 2 points for defending the flag, and 1 point for returning the flag. Overtime will only trigger if the round ends in a tie. 

## Living Daylights

> cname: livingdaylights \
> Teamplay: Toggleable

Living Daylights is a throwback to a scenario from GoldenEye 007. In this scenario, flags are strewn about the map when the round starts. Your objective is to grab a flag and hold onto it as long as you can; that is the only way to score points.

While holding the flag you can collect ammo, weapons, and armor to increase the rate at which you obtain points, however your only weapon is the flag itself! You do receive a speed bonus while holding the flag to avoid conflict with other players. If you do happen to receive damage, a timer begins that awards you with extra points and armor if you do not receive additional damage during the countdown.

When you pickup the flag your health and armor are refilled to the maximum.

## Gun Trade

> cname: guntrade \
> Teamplay: Never

Each player starts with a randomly selected weapon from a pool of 24 weapons available in game (Timed, Proximity or Remote Mines are not available in this mode). Upon killing an opponent, the 2 combatants swap weapons with each other.

Killing players with the Slapper will remove the victim's weapon from rotation, and assign a new weapon from the available pool of currently unused weapons to the attacker. 

## A View to A Kill

> cname: viewtoakill \
> Teamplay: Never

The goal of A View to A Kill is to stay alive as long as possible. You are scored on the duration of your longest life, or “inning”, rather than your kill count. Killing a player adds 50% of their current inning time to yours, but dying will reset your timer to 0.

Killing an opponent with the Slapper will award you with 75% of their current inning time, rather than 50%.

If a player's current inning time is above 1 minute, and within 75% of the leading players inning time, they will be highlighted with a red marker. This makes them alluring targets for others.

## Arsenal

> cname: arsenal \
> Teamplay: Never

Arsenal is a gameplay scenario that was inspired by a Counter Strike: Source modification. Each player starts off at the the same level/weapon and progresses down the list of 8 weapons in the current set. Each weapon requires 2 kills to advance to the next, with each weapon becoming more difficult to secure kills with. The first player to pass the final weapon is declared the victor. 