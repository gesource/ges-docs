## Page Formatting

Pages are written in Markdown, with some additions. Here are some shortcodes and formatting tricks used throughout the site.

### Lightbox

You can create a link that, when clicked, shows an image:

`[This is a link.](example.jpg)`

You can also create an image that, when clicked, shows the linked image in the lightbox. This is nice because it allows you to have a low-res preview image which opens the full-size image when clicked.

`[![A clickable image](small.jpg)](large.jpg)`

### Text Formatting

#### Article Date

`{{<article_date>}}` puts the `date` of the page into the article. It's just helpful to keep it in sync and not have to retype it.

#### Tables

Tables use 100% of the container (page) width. Sometimes that isn't appropriate and you may wish the table to fit the content instead.

Use the `{{<table-fit-content>}}` shortcode immediately prior to your table:

```
{{<table-fit-content>}}
| This is | a table |
|:--------|---------|
| Example | Content |
```

#### Line break

You can use `{{<br>}}` to insert a linebreak `<br />`. This is useful because Hugo's Markdown doesn't support linebreaks in table cells.

```
| This is a table   | with linebreaks          |
|:------------------|:-------------------------|
| This cell doesn't | But this cell{{<br>}}does|
```

#### Description

`{{<description>}}` puts the `description` of the page into the article. It's just helpful to keep it in sync and not have to retype it.

#### Subscript

`{{<sub>}}This text is subscript.{{</sub>}}`

#### Superscript

`{{<sup>}}This text is superscript.{{</sup>}}`

### Image Formatting

You can add these to the end of an image URL to activate special formatting options. These options are part of the Relearn theme used by this site.

The options are added to the end of the image URL like a [query string](https://en.wikipedia.org/wiki/Query_string), where the first option is prepended with a `?` and every option after that is prepended with a `&`. For example:

`![Alt text](example.jpg?width=16px&height=16px&classes=inline)`

* width
* height
* classes \*
  * left
  * right
  * inline

*\* this list is not exhaustive; these are just the most relevant ones*

You can use multiple classes by separating them with commas:

`![Alt text](example.jpg?width=16px&height=16px&classes=inline,left)`

You can use percentages for height/width, but the % sign needs to be replaced with `%25`. The below example sets the width to 33%:

`![Alt text](example.jpg?width=33%25&classes=inline,left)`

### Category-Specific Formatting

There are some shortcodes that are specific to certain pages or categories.

#### Character Info

This adds the character info table to the character pages.

The image at the top of the table is automatically picked from `/characters/portraits/<page_name>.jpg`


```
{{<character-info
  name="James Bond"
  aliases="007"
  occupation="00 Agent, British Intelligence"
  affiliation="MI6"
  height="6'1\""
  nationality="British"
  status="Active"
>}}
```

* `name`: The character's full name
* `aliases`: The alias(es) or alternative names of the character
* `occupation`: The character's occupation
* `affiliation`: Who the character works for
* `height`: The character's height
* `nationality`: The character's nationality
* `status`: The character's status, i.e. "Active" or "Deceased"

#### Map Info

This adds the map info table to the map (level) pages.

```
{{<map-info
    img-thumb="small.jpg"
    img-large="large.jpg"
    mapname="ge_archives"
    author="Luchador"
    first-release="Beta 1"
    location="St. Petersburg, Russia"
    playercount="10-16"
>}}
```

* `img-thumb`: Path to the screenshot thumbnail. This one MUST be specified or no image will appear in the table
* `img-large`: Path to the full-size screenshot. Displayed when clicking on the thumbnail
* `mapname`: File name (minus the .bsp)
* `author`: Who made it
* `first-release`: First GE:S version to feature this map
* `location`: The in-universe location of this map
* `playercount`: Ideal number of players for this map

The following are also supported, intended for use on community map pages:

* `released`: Date the level was released (format YYYY-MM-DD)
* `updated`: Date this version of the level was released (format YYYY-MM-DD)
* `wip-thread`: WIP/development thread (link MUST begin with `https://forums.geshl2.com/`)
* `release-thread`: Release thread (link MUST begin with `https://forums.geshl2.com/`)
* `download-link`: (not used) Where to download the level -- if ommitted, user will see message to download from the release thread

#### Weapon Info

This adds the weapon info table to the weapon pages.

`{{<weapon-info key="value" >}}`

All available options:

* `img`: Path to the image file for the weapon
* `ammo-capacity`: Ammo capacity of the weapon (without reloading)
* `ammo-class`: Type of ammo consumed by the weapon, i.e. Pistol, Rifle, Magnum, ...
* `damage`: Bars of damage the gun deals. This option is for explosives, etc that don't necessarily have limb-based damage
* `damage-head`: Bars of damage dealt by a headshot
* `damage-torso`: Bars of damage dealt by a shot to the torso
* `damage-leg`: Bars of damage dealt by as shot to the legs
* `fuse`: Grenades, timed mines: Amount of time before detonation
* `fire-rate`: How many rounds per second are shot by automatic weapons
* `fire-rate-hold`: How many rounds per second are shot by semi-automatic weapons when holding the trigger
* `fire-rate-tap`: Maximum rounds per second shot by semi-automatic weapons when tapping the trigger
* `accuracy`: Textual description of the accuracy of the weapon

Not all options are necessary as some are specific to certain types of weapons.

Here is an example pulled from the page for the Automatic Shotgun:

```
{{<weapon-info
    img             = "/weapons/images/autoshotgun.png"
    ammo-class      = "Shotgun"
    damage-head     = "2.4"
    damage-torso    = "1.68"
    damage-leg      = "0.6"
    ammo-capacity   = "5"
    fire-rate-hold  = "1.38"
    fire-rate-tap   = "1.75"
    accuracy        = "Terrible"
    special         = "High recoil,{{<br>}}Penetration"
>}}
```

#### Weaponset Metadata

This adds the weaponset info table to the weaponset pages.

`{{<weaponset-metadata key="value" >}}`

* `name`: Unique name of the weapon set
* `weight`: Weight of the weapon set (how likely it is to be picked by the game) 
* `category`: Category of the weapon set

Here is an example pulled from the page for Assault Weapons:

```
{{<weaponset-metadata
  name="assault_weapons"
  weight="500"
  category="Default_Sets"
>}}
```

