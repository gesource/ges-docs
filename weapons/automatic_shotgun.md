---
title: "Automatic Shotgun"
weight: 180
---

{{<weapon-info
	img				= "/weapons/images/autoshotgun.png"
	ammo-class		= "Shotgun"
	damage-head		= "2.4"
	damage-torso	= "1.68"
	damage-leg		= "0.6"
	ammo-capacity	= "5"
	fire-rate-hold	= "1.38"
	fire-rate-tap	= "1.75"
	accuracy		= "Terrible"
	special			= "High recoil,{{<br>}}Penetration"
>}}

The Automatic Shotgun is devastating in close quarters, but loses effectiveness at medium range. Great for ambushes and attacking players who are weakened but carry armor.

The Automatic Shotgun's real-world counterpart is the [SPAS-12](http://en.wikipedia.org/wiki/Spas12).

## Changes from GoldenEye 007

* The Automatic Shotgun has received a damage buff to accomodate the difficulty of getting into appropriate range and the improved usability of bullet weapons in the PC environment.
* The model has been changed to reflect a real world gun more.
* The Automatic Shotgun can now penetrate.

## GoldenEye 007

In the original game the Automatic Shotgun could be found in the following singleplayer missions:

* Statue
* Caverns

## Related Weapons

* [Shotgun](shotgun)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)