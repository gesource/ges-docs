---
title: "Gold PP7"
weight: 70
---

{{<weapon-info
	ammo-class		= "Pistol"
	damage-head		= "16"
	damage-torso	= "4"
	damage-leg		= "2"
	ammo-capacity	= "7"
	fire-rate-hold	= "1.5"
	fire-rate-tap	= "8.8"
	accuracy		= "Excellent"
>}}

The Gold PP7 is a variant of Bond's preferred weapon in many of the films. This golden variant twice again as powerful, making the Gold PP7 headshot a sure-kill. The Gold PP7 only appears in random and server-custom loadouts.

## Changes from GoldenEye 007

The Gold PP7 originally had a damage range of 1250 - 5000, just like the Golden Gun. It has been brought in sequence with normal and Silver PP7 to make it a playable weapon in online multiplayer competition. The Gold PP7 has also been changed to use Golden Gun bullets instead of pistol (9mm) ammo. 

## GoldenEye 007

The Gold PP7 was not featured in any of the missions in the original game, as it was only available using cheats. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [PP7](../pp7)
* [PP7 (Silenced)](../pp7_silenced)
* [Silver PP7](../silver_pp7)