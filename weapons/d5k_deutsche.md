---
title: "D5K Deutsche"
weight: 120
---

{{<weapon-info
	img				= "/weapons/images/d5k.png"
	ammo-class		= "Pistol"
	damage-head		= "2"
	damage-torso	= "0.5"
	damage-leg		= "0.25"
	ammo-capacity	= "30"
	fire-rate		= "8.0"
	accuracy		= "Very good"
>}}

The D5K Deutsche is a little slower than the [ZMG](zmg), but it does the same damage and has greater accuracy. While it is louder than its silenced counterpart, it is more accurate, making sniping easier.

The D5K Deutsche's real-world counterpart is the [MP5K](http://en.wikipedia.org/wiki/MP5#MP5K).

## GoldenEye 007

In the original game the D5K Deutsche could be found in the following singleplayer missions:

* Facility
* Frigate - dual wieldable
* Depot - dual wieldable
* Train
* Control

## Trivia

* 'Deutsche' means 'German', as the MP5K is a German firearm.
* In GoldenEye 007 the D5K Deutsche used the same firing sound as the Golden Gun.

## Related Weapons

* [D5K (Silenced)](d5k_silenced)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)