---
title: "Klobb"
weight: 90
---

{{<weapon-info
	img				= "/weapons/images/klobb.png"
	ammo-class		= "Pistol"
	damage-head		= "1.5"
	damage-torso	= "0.75"
	damage-leg		= "0.375"
	ammo-capacity	= "20"
	fire-rate		= "1.5"
	accuracy		= "Awful"
>}}

The Klobb is deliberately the worst gun in the game. It's slow, it sprays like a shotgun, and sometimes hitting someone with it practically extends their lifespan. None the less, it can provide cover fire, it can get lucky from time to time, and against the Golden Gun carrier, he won't be able to recover from the scratches with armor, so you may as well let it fly.

The Klobb's real-world counterpart is the [Škorpion vz. 61](http://en.wikipedia.org/wiki/Skorpion).

## Changes from GoldenEye 007
Developers and Beta Testers automatically receive the Golden Klobb and Midnight Klobb, respectively. The weapon is statistically the same but cosmetically different.

## GoldenEye 007
In the original game the Klobb could be found in the following singleplayer missions:

* Runway - dual wieldable
* Surface I - dual wieldable
* Bunker I - dual wieldable
* Surface II - dual wieldable
* Bunker II - dual wieldable
* Archives - dual wieldable

## Trivia
The Klobb is named after [Ken Lobb](http://en.wikipedia.org/wiki/Ken_Lobb), a developer of the original game.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)