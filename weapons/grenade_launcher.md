---
title: "Grenade Launcher"
weight: 270
---

{{<weapon-info
	img				= "/weapons/images/grenade_launcher.png"
	ammo-class		= "Grenade Launcher Round"
	ammo-capacity	= "6"
	fire-rate		= "1.05"
	special			= "Delayed fire"
>}}

 The Grenade Launcher is a devestating weapon in the right hands. The ability to bank rounds off surfaces means that the cover you're behind might not be as safe as you think. Careful, many players have less discretion than this weapon has ammo, and will fill a room with explosions that don't hurt anyone but themselves.

The Grenade Launcher's real-world counterpart is the [TGL-6](http://combinedsystems.com/pennarms/40mm-Multi5.aspx).

## GoldenEye 007

In the original game the Grenade Launcher could be found in the following singleplayer missions:

* Surface I
* Streets
* Jungle

## Trivia

In Jungle after you defeat Xenia and pick up her weapons, you can wield both the Grenade Launcher and RC-P90 together. This is the only time in the game where you can dual wield two different weapons without using the weapon switch bug. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)