---
title: "Hand Grenade"
weight: 220
---

{{<weapon-info
	img				= "/weapons/images/grenade.png"
	fuse			= "4 seconds"
	ammo-class		= "Grenade"
	fire-rate		= "1.2"
	special			= "Throwable"
>}}

The Hand Grenade will detonate four seconds after the pin is pulled. It deflects easily off surfaces, and can be used to clear out those hard-to-reach places. Just don't forget to throw it.

The Hand Grenade's real-world counterpart is the [Mk 2 Grenade](http://en.wikipedia.org/wiki/Mk_2_grenade).

## GoldenEye 007

In the original game, the Hand Grenade could be found in all missions by killing enemies using them before they are thrown. 

## Trivia

In Secret Agent and 00 Agent, it was possible for scientists to pull out grenades after the player had shot them twice.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)