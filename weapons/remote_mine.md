---
title: "Remote Mine"
weight: 250
---

{{<weapon-info
	img				= "/weapons/images/remotemine.png"
	ammo-class		= "Remote Mine"
	fire-rate		= "1.2"
	special			= "Throwable,{{<br>}}Sticks to surfaces,{{<br>}}Remotely detonated"
>}}

Remote Mines stick to surfaces and detonate when triggered by the owner's watch detonator. Alternatively you can quick-detonate them by pressing primary and secondary fire at the same time.

Mines can be triggered in mid-air before attaching to a surface, making them as useful as a short-range grenade launcher as they are for booby-trapping.  

## GoldenEye 007

In the original game Remote Mines could be found in the following singleplayer missions:

* Facility - starting weapon
* Surface II - starting weapon
* Jungle - starting weapon
* Control

## Related Weapons

* [Timed Mine](timed_mine)
* [Proximity Mine](proximity_mine)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)