---
title: "Throwing Knife"
weight: 30
---

{{<weapon-info
	img				= "/weapons/images/knife.png"
	ammo-class		= "Throwing Knife"
	damage-head		= "12"
	damage-torso	= "6"
	damage-leg		= "3"
	fire-rate		= "1.0"
	special			= "Throwable"
>}}

The Throwing Knife provides a powerful and silent short-range attack, although aiming is quite difficult. 

## Changes from GoldenEye 007

Holding fire will not prepare the knife to be thrown. . 

## GoldenEye 007

In the original game the Throwing Knife could be found in the following singleplayer missions:

* Bunker II

## Trivia

Throwing Knives could only be acquired in Bunker II by using your Watch Magnet Attract above the drain in the prison. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [Hunting Knife](hunting_knife)