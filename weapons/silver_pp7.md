---
title: "Silver PP7"
weight: 60
---

{{<weapon-info
	ammo-class		= "Magnum"
	damage-head		= "8"
	damage-torso	= "2"
	damage-leg		= "1"
	ammo-capacity	= "7"
	fire-rate-hold	= "1.5"
	fire-rate-tap	= "8.8"
	accuracy		= "Excellent"
	special			= "Penetration"
>}}

The Silver PP7 is a variant of Bond's preferred weapon in many of the films. This silver variant is twice as powerful as the normal PP7. 

## Changes from GoldenEye 007

The Silver PP7 has been changed to use magnum bullets instead of pistol (9mm) ammo.

## GoldenEye 007

The Silver PP7 was not featured in any of the missions in the original game, as it was only available using cheats. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [PP7](../pp7)
* [PP7 (Silenced)](../pp7_silenced)
* [Gold PP7](../gold_pp7)