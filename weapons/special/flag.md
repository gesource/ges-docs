---
title: "Flag"
---

{{<weapon-info
	damage			= "6"
	fire-rate		= "1.42"
	special			= "Gameplay token,{{<br>}}Melee"
>}}

The Flag is a gameplay token used for Capture the Flag and The Living Daylights. When held, it can be used as a melee weapon

## Gameplay

* In Capture The Flag:
  * Players must capture the enemy's flag and bring it to their own capture point to score a point
* In The Living Daylights:
  * Players must pick up and hold flags to score points