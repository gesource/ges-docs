---
title: "Scenario-Specific Weapons"
---

This section lists special weapons that are specific to certain gameplay scenarios.

{{% children  %}}