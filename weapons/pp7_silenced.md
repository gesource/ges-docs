---
title: "PP7 (Silenced)"
weight: 50
---

{{<weapon-info
	img				= "/weapons/images/pp7_silenced.png"
	ammo-class		= "Pistol"
	damage-head		= "4"
	damage-torso	= "1"
	damage-leg		= "0.5"
	ammo-capacity	= "7"
	fire-rate-hold	= "1.5"
	fire-rate-tap	= "8.8"
	accuracy		= "Good"
	special			= "Noise suppression,{{<br>}}No tracers"
>}}

The Silenced PP7 is a variant of Bond's preferred weapon in many of the films. The silenced version is less accurate than the unsilenced version, but the shot sound is supressed, and the bullet does not leave a tracer.

The Silenced PP7's real-world counterpart is the [Walther PPK](http://en.wikipedia.org/wiki/Walther_PP) with a detachable silencer.

## GoldenEye 007

In the original game the Silenced PP7 could be found in the following singleplayer missions:

* Dam - starting weapon
* Facility - starting weapon
* Runway - starting weapon
* Surface I - starting weapon
* Bunker I - starting weapon
* Silo - starting weapon
* Surface II - starting weapon
* Bunker II - dual wieldable

## Trivia

In Bunker 2, dual Silenced PP7's could be found in the safe if you collected both of the safe keys in the level.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [PP7](../pp7)
* [Silver PP7](../silver_pp7)
* [Gold PP7](../gold_pp7)