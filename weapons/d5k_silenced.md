---
title: "D5K (Silenced)"
weight: 130
---

{{<weapon-info
	ammo-class		= "Pistol"
	damage-head		= "2"
	damage-torso	= "0.5"
	damage-leg		= "0.25"
	ammo-capacity	= "30"
	fire-rate		= "8.0"
	accuracy		= "Good"
	special			= "Noise reduction,{{<br>}}No tracers"
>}}

The Silenced D5K is a little slower than the ZMG, but does the same damage and has similar accuracy. The silenced version is less accurate than the unsilenced version, but the shot sound is muffled, and the bullet does not leave a tracer.

The Silenced D5K's real-world counterpart is the [MP5K](http://en.wikipedia.org/wiki/MP5#MP5K) with a detachable silencer. 

## GoldenEye 007

In the original game the Silenced D5K could be found in the following singleplayer missions:

* Frigate - starting weapon

## Related Weapons

* [D5K Deutsche](d5k_deutsche)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)