---
title: "Timed Mine"
weight: 230
---

{{<weapon-info
	fuse			= "4 seconds"
	ammo-class		= "Timed Mine"
	fire-rate		= "1.2"
	special			= "Throwable,{{<br>}}Sticks to surfaces"
>}}

Timed Mines are quite similar to the [Grenade](grenade), with the exception that they stick to whichever surface they land on.

## GoldenEye 007

In the original game Timed Mines could be found in the following singleplayer missions:

* Runway
* Caverns - starting weapon

## Related Weapons

* [Proximity Mine](proximity_mine)
* [Remote Mine](remote_mine)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)