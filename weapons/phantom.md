---
title: "Phantom"
weight: 140
---

{{<weapon-info
	img				= "/weapons/images/phantom.png"
	type			= "Gun"
	ammo-class		= "Pistol"
	damage-head		= "1.9"
	damage-torso	= "1.425"
	damage-leg		= "0.475"
	ammo-capacity	= "50"
	fire-rate		= "9.1"
	accuracy		= "Good"
	special			= "Medium recoil"
>}}

The Phantom is a powerful SMG in weapon loadouts that need some extra muscle. She responds well to a gentle touch, losing accuracy when firing is sustained. Likes cats, long walks on the beach, and painting the walls red in close-quarters combat.

The Phantom's real-world counterpart is the [Spectre M4](http://en.wikipedia.org/wiki/Spectre_M4).

## GoldenEye 007

In the original game the Phantom could be found in the following singleplayer missions:

* Frigate - dual wieldable

In GoldenEye 007, the Phantom used the same firing sound as the KF7 Soviet.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)