---
title: "Shotgun"
weight: 170
---

{{<weapon-info
	img				= "/weapons/images/shotgun.png"
	ammo-class		= "Shotgun"
	damage-head		= "1.75"
	damage-torso	= "1.4"
	damage-leg		= "0.875"
	ammo-capacity	= "5"
	fire-rate-hold	= "1.38"
	fire-rate-tap	= "1.75"
	accuracy		= "Terrible"
	special			= "Penetration"
>}}

The Shotgun is useful in close quarters, but loses effectiveness at medium range. It is powerful enough to attack an un-armored enemy, but can be out-gunned by a quality weapon.

The Shotguns's real-world counterpart is the [Remington 870](http://en.wikipedia.org/wiki/Remington_870).

## Changes from GoldenEye 007

The Shotgun has received a damage buff to accommodate the difficulty of getting into appropriate range and the improved usability of bullet weapons in the PC environment.

## GoldenEye 007

The Shotgun was not featured in any of the missions in the original game, as it was only available using cheats.

## Related Weapons

* [Automatic Shotgun](automatic_shotgun)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)