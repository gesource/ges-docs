---
title: "ZMG"
weight: 110
---

{{<weapon-info
	img				= "/weapons/images/zmg.png"
	ammo-class		= "Pistol"
	damage-head		= "1.4"
	damage-torso	= "0.875"
	damage-leg		= "0.35"
	ammo-capacity	= "32"
	fire-rate		= "12.8"
	accuracy		= "Good"
>}}

The ZMG is the model mid-grade sub-machine gun. It has average accuracy, median damage, and no special talents beyond the two extra bullets in its magazine.

The ZMG's real-world counterpart is the [Micro Uzi](http://en.wikipedia.org/wiki/Micro_Uzi).

## GoldenEye 007

In the original game the ZMG could be found in the following singleplayer missions:

* Train - dual wieldable
* Caverns - starting weapon & dual wieldable
* Cradle - dual wieldable
* Egyptian - dual wieldable

## Trivia

Early in GE:S development, it was confused with the D5K Deutsche and given a silencer. This mistake was fixed in Beta 3. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)