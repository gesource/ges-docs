---
title: "Moonraker Laser"
weight: 260
---

{{<weapon-info
	ammo-capacity	= "Infinite"
	damage-head		= "4"
	damage-torso	= "2"
	damage-leg		= "1"
	fire-rate-hold	= "3.3"
	fire-rate-tap	= "4.5"
	accuracy		= "Perfect"
	special			= "Penetration"
>}}

Pew pew lasers. Behaves like the Silver PP7 without the need to reload. 

## GoldenEye 007

In the original game the Moonraker Laser could be found in the following singleplayer missions:

* Aztec - dual wieldable

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)