---
title: "Sniper Rifle"
weight: 190
---

{{<weapon-info
	img				= "/weapons/images/sniper_rifle.png"
	ammo-class		= "Rifle"
	damage-head		= "4"
	damage-torso	= "2"
	damage-leg		= "1"
	ammo-capacity	= "8"
	fire-rate-hold	= "2.6"
	fire-rate-tap	= "3.4"
	accuracy		= "Perfect"
	special			= "Powerful zoom sighting,{{<br>}}Silenced,{{<br>}}Penetration"
>}}

The Sniper Rifle provides the steady hand with unparalleled long-range combat capability, but is no slouch in close quarters, where it can hold its own against powerful pistols and mid-level sub-machine guns.

The Sniper Rifle's real-world counterpart is the [KSVK 12.7](http://en.wikipedia.org/wiki/Ksvk).

## Changes from GoldenEye 007

Damage has been increased to differentiate the Sniper Rifle from the similar Silenced PP7 and ensure it is competitive with sub-machine guns and other rifles in the PC environment.

## GoldenEye 007

In the original game the Sniper Rifle could be found in the following singleplayer missions:

* Dam
* Surface I - starting weapon

## Trivia

* In the original game when you had a Sniper Rifle, your Slappers were replaced with the Sniper Rifle buttstock as a melee weapon. This was a purely cosmetic change and was statistically the same as Slappers.
* A bug in the original game meant that quickly switching to Slappers after picking up a Sniper Rifle would instead give you an inverted arm model where the Sniper Rifle buttstock would be. This was referred to as the 'paintbrush' as it somewhat resembled a large paintbrush.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)