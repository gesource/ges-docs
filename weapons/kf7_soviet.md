---
title: "KF7 Soviet"
weight: 100
---

{{<weapon-info
	img				= "/weapons/images/kf7_soviet.png"
	ammo-capacity	= "30 rounds"
	ammo-class		= "Rifle"
	damage-head		= "2"
	damage-torso	= "1"
	damage-leg		= "0.5"
	fire-rate		= "8.5"
	accuracy		= "Good"
	special			= "Zoom sighting,{{<br>}}3-round burst or automatic"
>}}

The KF7 is a low-power burst-fire rifle that appears in weak weapon sets. Its ability to zoom gives it an advantage over equally-damaging pistols and submachine guns, but its inaccuracy makes it a poor sniping weapon over great distances.

The KF7 Soviet's real-world counterpart is the [AKS-74](http://en.wikipedia.org/wiki/AK-74#Variants).

## GoldenEye 007

In the original game the KF7 Soviet could be found in the following singleplayer missions:

* Dam
* Facility
* Runway
* Surface I
* Bunker I
* Silo
* Surface II
* Bunker II
* Statue
* Archives
* Streets
* Depot

## Trivia
* In the movie the KF7 Soviet is actually a modified [Type 56](http://en.wikipedia.org/wiki/Type_56), a Chinese immitation of the AKS-74.
* In Depot the KF7 can be found in the arms cache, but there is no ammunition available for it.
* In Goldeneye 007 the KF7 Soviet used the same firing sound as the Phantom.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)