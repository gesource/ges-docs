---
title: "Cougar Magnum"
weight: 200
---

{{<weapon-info
	img				= "/weapons/images/cmag.png"
	ammo-class		= "Magnum"
	damage-head		= "8"
	damage-torso	= "4"
	damage-leg		= "2"
	ammo-capacity	= "6"
	fire-rate		= "1.5"
	accuracy		= "Perfect"
	special			= "Delayed fire,{{<br>}}Penetration"
>}}

The Cougar Magnum is the second most accurate pistol and third most accurate weapon in the game. It is surpassed only by the Sniper Rifle and the Golden Gun. Because it is a revolver, there is a brief delay for the chambers to revolve before the bullet is fired.

The Cougar Magnum's real-world counterpart is the [Ruger Blackhawk](http://en.wikipedia.org/wiki/Ruger_Blackhawk).

## Changes from GoldenEye 007

The Cougar Magnum was not featured in any of the missions in the original game, as it was only available using cheats.

## GoldenEye 007

Whilst the Cougar Magnum was wielded by Natalya in the Jungle and Control missions, it could not be picked up if she died.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)