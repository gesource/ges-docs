---
title: "Proximity Mine"
weight: 240
---

{{<weapon-info
	img				= "/weapons/images/proxymine.png"
	ammo-class		= "Proximity Mine"
	fire-rate		= "1.2"
	special			= "Throwable,{{<br>}}Motion activated"
>}}

Proximity Mines, shortly after being attached to a surface, will detonate if a player is nearby. They are sensitive to velocity, which allows you to crouch or walk (aim-mode) past them without triggering detonation. 

## Changes from GoldenEye 007

* Because players have a brief period of protection at spawn, Proximity Mines planted within trigger-range of a player spawn will detonate but deal no damage.
* Proximity mines will emit a beeping sound as you come into range of triggering them, helping to alert you to their whereabouts.
* Proximity mines are not triggered if you move slowly passed them.

## GoldenEye 007

In the original game Proximity Mines could be found in the following singleplayer missions:

* Depot

## Related Weapons

* [Timed Mine](timed_mine)
* [Remote Mine](remote_mine)

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)