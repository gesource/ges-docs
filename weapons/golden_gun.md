---
title: "Golden Gun"
weight: 210
---

{{<weapon-info
	img				= "/weapons/images/golden_gun.png"
	ammo-class		= "Golden Bullets"
	damage-head		= "400"
	damage-torso	= "200"
	damage-leg		= "100"
	ammo-capacity	= "1"
	fire-rate		= "0.79"
	accuracy		= "Perfect"
>}}

The Golden Gun is the most powerful weapon in GoldenEye: Source, always deadly and perfectly accurate when sighted.

The Golden Gun appears both in The Man With The Golden Gun scenario and in the Golden Gun weapon loadout. It can also appear in other gameplay scenarios such as Arsenal, where it is a starting weapon in some loadouts.

## Changes from GoldenEye 007

* Under Man With The Golden Gun scenario, the player carrying it could not pick up armor. In GE:S, you may pick up armor, but only if you never had armor during the current inning. Once you gain armor, the background of the armor status dial turns red, indicating you cannot pick up armor again. This change ensures that players will not prioritize armor over the Golden Gun.
* Under Man With The Golden Gun scenario, all ammunition crates will provide golden bullets. This is to discourage camping the Golden Gun spawn location simply for ammunition and ensure that a player who wins the Golden Gun from an enemy can get ammunition without returning to the Golden Gun spawn area.

## GoldenEye 007

In the original game the Golden Gun could be found in the following singleplayer missions:

* Egyptian

## Trivia
* The Golden Gun is sometimes called the AuG - 'Au' being the periodic symbol for gold.
* In GoldenEye 007 the Golden Gun used the same firing sound as the [D5K Deutsche](d5k_deutsche).

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)