---
title: "PP7"
weight: 40
---

{{<weapon-info
	img				= "/weapons/images/pp7.png"
	ammo-class		= "Pistol"
	damage-head		= "4"
	damage-torso	= "1"
	damage-leg		= "0.5"
	ammo-capacity	= "7"
	fire-rate-hold	= "1.5"
	fire-rate-tap	= "8.8"
	accuracy		= "Excellent"
>}}

The PP7 is Bond's preferred weapon in many of the films. While it is louder than its silenced counterpart, it is more accurate, making sniping easier. The PP7's real-world counterpart is the [Walther PPK](http://en.wikipedia.org/wiki/Walther_PP). 

## GoldenEye 007

In the original game the PP7 could be found in the following singleplayer missions:

* Bunker I
* Frigate - starting weapon
* Statue - starting weapon
* Archives
* Streets - starting weapon
* Depot - starting weapon
* Train - starting weapon
* Jungle - starting weapon
* Control - starting weapon
* Caverns - starting weapon
* Cradle - starting weapon
* Aztec - starting weapon
* Egyptian - starting weapon

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [PP7 (Silenced)](../pp7_silenced)
* [Silver PP7](../silver_pp7)
* [Gold PP7](../gold_pp7)