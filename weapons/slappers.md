---
title: "Slappers"
weight: 10
---

{{<weapon-info
	ammo-class 		= "None"
	damage-head		= "4"
	damage-torso	= "3"
	damage-leg		= "2"
	fire-rate		= "1.42"
	special			= "Melee"
>}}

Slappers are the default melee attack.

## Changes from GoldenEye 007

Melee damage is calculated differently in GoldenEye: Source, due to a different hitbox system.

## GoldenEye 007

Slappers were available as a starting weapon on every mission and in every weapon set.

## Trivia

* When viewing your weapons list in your watch, it refers to the Slappers as 'Unarmed'.
* The only time in GoldenEye 007 refers to the unarmed attack as Slappers is in the weapon loadout selection of Multiplayer.
* In the original game when you had a Sniper Rifle, your Slappers were replaced with the Sniper Rifle buttstock as a melee weapon. This was a purely cosmetic change and was statistically the same as Slappers.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)