---
title: "Hunting Knife"
weight: 20
---

{{<weapon-info
	img				= "/weapons/images/knife.png"
	damage-head		= "6"
	damage-torso	= "4.5"
	damage-leg		= "3"
	fire-rate		= "1.42"
	special			= "Melee"
>}}

The Hunting Knife is a melee weapon that can appear in weapon sets.

## Changes from GoldenEye 007

Melee damage is calculated differently in GoldenEye: Source, due to a different hitbox system. 

## GoldenEye 007

The Hunting Knife was not featured in any of the missions in the original game, as it was only available using cheats. 

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)

## Related Weapons

* [Throwing Knife](throwing_knife)