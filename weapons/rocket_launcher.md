---
title: "Rocket Launcher"
weight: 280
---

{{<weapon-info
	img				= "/weapons/images/rocket_launcher.png"
	ammo-capacity	= "1"
	ammo-class		= "Rocket"
	fire-rate		= "0.7"
>}}

Puts explosions where you want them. A direct hit will kill anyone; splash and flames will work on most players without armor as long as you are close.

The Rocket Launcher's real-world counterpart is the [Type 69 RPG](http://en.wikipedia.org/wiki/Type_69_RPG).

## Changes from GoldenEye 007

You can carry up to 7 rockets instead of 4, making ammo camping unnecessary.

## GoldenEye 007

In the original game the Rocket Launcher could be found in the following singleplayer missions:

* Streets
* Depot


## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)