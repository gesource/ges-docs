---
title: "RC-P90"
weight: 160
---

{{<weapon-info
	img				= "/weapons/images/rc-p90.png"
	ammo-class		= "Pistol"
	damage-head		= "1.8"
	damage-torso	= "0.9"
	damage-leg		= "0.45"
	ammo-capacity	= "80"
	fire-rate		= "12.8"
	accuracy		= "Excellent"
	special			= "Penetration"
>}}

Everyone's favorite lead-slinger, the RC-P90 is here to do one thing: deal massive amounts of damage very quickly. Just don't get cocky and start spamming, because a DD44 will kill you if he aims and you don't.

The RC-P90's real-world counterpart is the [FN P90](http://en.wikipedia.org/wiki/FN_P90).

## GoldenEye 007

In the original game the RC-P90 could be found in the following singleplayer missions:

* Train
* Jungle
* Caverns - dual wieldable

## Trivia

* The RC-P90 can be found in the double-stacked crate at the end of the first carriage on Train in Agent.
* In Jungle after you defeat Xenia and pick up her weapons you can wield both the RC-P90 and Grenade Launcher together, this is the only time in the game where you can dual wield two different weapons without using the weapon switch bug.
* In Caverns, after Trevelyan escapes in the elevator, guards will show up with dual RC-P90's which you can take.

## References

* [David Wonn's Goldeneye Weapon Analysis](http://kontek.net/davidwonn/ge007stats.html)