---
title: "Map Script Files"
---

GE:S uses script files to determine critical gameplay information about how maps and gameplays are selected. These allow for full customization of your server map and gameplay rotation.

A map script file should go in `scripts/maps` and use the exact map name as its filename, minus extension (`ge_archives.bsp` becomes `ge_archives.txt`). All files there by default can be changed on the whims of the server owner, with `default.txt` applying to all maps that don't explicitly override its settings, and all maps that do not have a map script file.

## Parameters

|Parameter|Description|
|:--------|-----------|
|BaseWeight| The weight the map has at its ideal playercount |
|MaxPlayers| Highest playercount on the server before the map will no longer be considered for selection|
|MinPlayers| Lowest playercount on the server before the map will no longer be considered for selection|
|ResIntensity| Amount of memory the map uses, used to determine when to take measures to prevent client crashes.|
|TeamThreshold| Playercount required before teamplay gets enabled on teamplay capable modes.|
|WeaponsetWeights| Weaponset names contained in the brackets following this keyword will have their random selection weights overridden with whatever is specified. If they are not included at all in a given gameplay's loadout list, the weight will not be overridden.|
|GamemodeWeights| The random selection weights of the gamemodes you want to be played on your map. If a gamemode is listed in default.txt it will use that weight on all maps that do not specify a new weight. If a gamemode is not specified at all it will not come up in rotation.|
|TeamGamemodeWeights| Same as gamemode weights, but this selection pool will only be used if the map is switched to when the playercount is above its team threshold.|
