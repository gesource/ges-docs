---
title: "Fast Download Server"
---

A fast download server (also known as a FastDL server) is a website that mirrors the custom content on your server. When a client connects, they download the custom files from the website instead of the game server directly. This results in faster downloads without slowing down your game server.

It is **strongly** encouraged that any server with custom assets utilize a fast download server. Downloads directly from the game server are **slow**.

## How It Works

1. Client connect: Game server sends the client a [list of files](/server/adding-content/levels/#write-a-download-list) that you need to have

1. Determine download server: The client determines where to get the files by the `sv_downloadurl` setting on the server

1. The client downloads the file:
   * If `sv_downloadurl/file_path/file.bz2` exists, the client downloads that and decompresses it

   * If `sv_downloadurl/file_path/file.bz2` _does not_ exist, it downloads `sv_downloadurl/file_path/file` instead

   * The client does this for each missing file listed by the server

1. The client finishes connecting to the server


## Setup (Web server)

To set up a FastDL server, all you need is a website on the Internet capable of serving binary files.

The exact steps are beyond the scope of this article, but it can be done using Microsoft IIS on Windows, Apache, or your Web server of choice. You can also use cloud services like Amazon S3, Azure Blob Storage, OpenStack Swift, ...

## Adding Files to Your FastDL Server

Create a folder on your webserver that will represent the `gesource` directory. In this example, we will create the folder `/fastdl` under the root of our website.

Thus, our Fast Download URL is as follows: `https://www.example.com/fastdl/`

Then, we can start putting our custom content here.

In our example, we will use the [Void](https://forums.geshl2.com/index.php/topic,8890.0.html) level created by Entropy-Soldier. 

The level download includes the following files:

```
maps
├── ge_void_a2.bsp
└── ge_void_a2.res

materials
└── VGUI
    └── loadingscreens
        ├── ge_void_a2_widescreen.vmt
        └── ge_void_a2_widescreen.vtf

scripts
├── maps
│   └── ge_void_a2.txt
├── music
│   └── level_music_ge_void_a2.txt
└── soundscapes_ge_void_a2.txt

sound
└── music
    ├── mv_control.mp3
    └── pd_datadyneresearch.mp3
```

*Each* of the files and folders included with the custom map will need to be uploaded to the `fastdl` folder we created in our example (except for the [.res](/server/adding-content/levels/#the-hard-way-using-the-inbuilt-system) file -- that is only needed by the game server!)

### BZ2 Compression

Source engine games support BZ2 (de)compression when fetching files from FastDL servers. This is recommended as some types of content, like BSP files, are highly compressible.

It's recommended for all files, even those that don't benefit from compression, because the game client tries to download the BZ2 file before it downloads the original file.

To compress your FastDL files, simply compress each individual file into a .bz2. When done, our directory tree on the FastDL server should look like this:

```
maps
└── ge_void_a2.bsp.bz2

materials
└── VGUI
    └── loadingscreens
        ├── ge_void_a2_widescreen.vmt.bz2
        └── ge_void_a2_widescreen.vtf.bz2

scripts
├── maps
│   └── ge_void_a2.txt.bz2
├── music
│   └── level_music_ge_void_a2.txt.bz2
└── soundscapes_ge_void_a2.txt.bz2

sound
└── music
    ├── mv_control.mp3.bz2
    └── pd_datadyneresearch.mp3.bz2
```

{{% notice style="tip" %}}
You can use the scripts attached to this article to easily BZ2-compress an entire directory of files. Just download the scripts and read the README.

*\* requires 7-Zip to be installed*
{{% /notice %}}

{{% attachments sort="asc" /%}}

## Setup (Game server)

In `server.cfg`, set `sv_downloadurl` to the URL of your website. Note you will need to put the URL in \"double quotes\", or the `//` will be parsed as a comment.

```
sv_downloadurl "https://www.example.com/fastdl/“
```

The change will take effect on the next map, or you can type the command into the server console for it to take effect right way.

That's all there is to it! Your clients will begin downloading the files from your FastDL server, provided your server has download lists for your custom maps.