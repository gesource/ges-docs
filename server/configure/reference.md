---
title: "Configuration File Reference"
weight: -100
---

This is a quick reference of where different configuration files live in GoldenEye: Source.

## CFG Files

These files are in the `cfg` folder. They execute commands in the order which they are listed in the file. However, different CFG files are executed in different orders and in different circumstances.

### Startup Scripts: Valve.rc and server.cfg

`Valve.rc` is executed only once: when the server first starts. This is for initial settings and commands, and generally is not modified by the server operator.

`server.cfg` is executed at each map load (after `Valve.rc`). Use `server.cfg` for your custom settings, and settings which need to be reset every time a map loads. By default, there are lots of comments in this file explaining what various settings do.

### Game Mode-Specific Configuration

After a map loads, a game mode is loaded, followed by executing the game mode's CFG file.

The CFG file for a game mode should be located at `cfg/<gameplay_name>.cfg`, where `<gameplay_name>` is the name of the game mode file, excluding the file extension. This is the same name you would put in the gameplay cycle, and the same name that you would get by running `ge_gameplaylist`. For example, if you wanted to use custom settings for License to Kill, you would create the file `cfg/ltk.cfg`.

If you change a setting for a specific game mode, be sure to add the command to set it back in `server.cfg`. Otherwise, the setting may persist after the map changes. For the below example, let's assume you want to enable Paintball Mode for License to Kill. In `server.cfg`, you have:

```
ge_paintball 0
```

In `ltk.cfg`, you have:

ge_paintball 1

In this example,

* server.cfg is executed when the map loads, running `ge_paintball 0`

* ltk.cfg then executes when the game mode loads, running `ge_paintball 1`

* When the match ends and a new map is loaded, `server.cfg` is executed again, running `ge_paintball 0`.

* The next game mode is loaded and the game executes its CFG file, if present.

## Map Cycle

As of v5.0, GoldenEye: Source uses a [map script](../map_scripts) system to choose which level to play next. These scripts are in `scripts/maps`.

You can fall back to the old mapcycle file by setting `ge_mapchooser_usemapcycle` to `1` (**strongly discouraged!**), in which case the level rotation is chosen from `mapcycle.txt` in the root `gesource` folder.

## Gameplay Cycle

As of v5.0, GoldenEye: Source uses a [map script](../map_scripts) system for choosing game mode based on weight as well as map.

`scripts/maps/default.txt` specifies the default gamemode weights. Other map scripts override the default game mode weights for that map only.

You can fall back to using the old gameplaycycle file by setting `ge_gameplay_mode` to `2`.