---
title: "Message of the Day"
---

The Message of the Day (or MOTD for short) is seen whenever a user first joins the server or changes teams. The MOTD can be either a webpage or plain text. The MOTD viewer uses an old version of MSHTML (Internet Explorer), so don't get too fancy with your MOTD.

The default MOTD file is located at `motd.txt`. This can be changed using the `motdfile` console command.

You can either include HTML inside that file or only include a link to an external webpage; in which case, the linked webpage will be loaded instead.

The ratio of the MOTD viewer is 3:1, and the size of the MOTD viewer is relative to the user's screen resolution. However, the webpage does not automatically resize. You can specify images to use percentages for their height/width to combat this. However, text scaling through CSS does not work.