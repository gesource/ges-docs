---
title: "Linux: High CPU Usage"
---

If you see abnormally high CPU usage on your WINE-based server, the most likely cause is that an X server is not running or is not properly defined. (If you're running headless, see our [Linux install guide](/server/getting-started/install/linux) for how to set up Xvfb). 