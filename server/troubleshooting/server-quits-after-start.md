---
title: "Server Quits Immediately After Start"
---

There are several known causes of this issue:

* Your `-game` path is incorrect. Your `-game` path needs to point to the `gesource` folder. If the path has spaces, ensure it is wrapped in \"double quotes\".

* Windows: You may be missing the 32-bit Microsoft Visual C++ 2012 redistributable, or mistakingly installed the 64-bit version instead.

* Linux: If you're using a WINE-based server, you may have defined the DISPLAY variable but aren't actually running an X server. If you're running a headless server, make sure Xvfb is running and that DISPLAY is pointing to the correct display.

