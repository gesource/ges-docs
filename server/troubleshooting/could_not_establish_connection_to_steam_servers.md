---
title: "Could not establish connection to Steam servers"
---

The effects of this error message is that:

* VAC is not enabled
* the game server will not be listed in the server browser, which is a significant handicap to public servers.

**If you just set up your server**: don't panic! It's normal to see this at first, while your server connects soon after.

You can check for connectivity by searching the console for occurrences of `VAC secure mode is activated`. Afterwards, upon restarting the server you should see the following:

```
Connection to Steam servers successful.
   VAC secure mode is activated.`
```

## Further Troubleshooting

If after a few hours your server does not connect to Steam, here's a few things to check:
config.vdf

Stop the server, open the file `[SrcDS Root]/config/config.vdf` (create this file/folder if it does not exist), and write the following data to the file. Upon restart, the server should immediately connect.

```
"InstallConfigStore"
{
	"Software"
	{
		"Valve"
		{
			"Steam"
			{
				"CurrentCellID"		"81"
				"PingTimeForCurrentCellID"		"0"
				"CM"		"162.254.192.87:27022;162.254.192.101:27019;162.254.192.109:27017;162.254.192.71:27020;162.254.192.71:27027;162.254.192.101:27020;162.254.192.71:27025;162.254.192.101:27017;162.254.192.100:27020;162.254.192.87:27024;162.254.192.71:27021;162.254.192.71:27023;162.254.192.87:27021;162.254.192.71:27017;162.254.192.71:27018;162.254.192.87:27026;162.254.192.87:27023;162.254.192.71:27026;162.254.193.74:27023;162.254.192.100:27018;162.254.193.74:27026;162.254.193.102:27019;162.254.193.74:27019;162.254.193.74:27022;162.254.193.102:27020;162.254.193.74:27017;162.254.193.74:27018;162.254.192.108:27017;162.254.192.108:27019;162.254.192.109:27021;162.254.192.109:27020;162.254.192.101:27021;162.254.192.87:27017;162.254.192.109:27019;162.254.192.100:27021;162.254.192.71:27019;162.254.192.101:27018;162.254.193.102:27021;162.254.193.102:27017;162.254.193.102:27025;162.254.193.102:27026;162.254.193.74:27021;162.254.193.74:27020;162.254.193.74:27025;162.254.193.102:27018;162.254.193.74:27027;162.254.193.102:27022;162.254.192.87:27018;162.254.192.87:27025;162.254.192.109:27018;162.254.192.87:27027;162.254.192.108:27020;162.254.192.100:27017;162.254.192.108:27018;162.254.192.100:27019;162.254.192.87:27019;162.254.192.108:27021;162.254.192.87:27020;162.254.192.71:27022;162.254.193.102:27027;162.254.193.102:27024;162.254.193.102:27023;162.254.193.74:27024;162.254.192.71:27024;162.254.195.87:27017;162.254.195.82:27017;162.254.195.87:27024;162.254.195.87:27022;162.254.195.67:27017;162.254.195.66:27019;162.254.195.87:27027;162.254.195.71:27025;162.254.195.71:27023;162.254.195.71:27024;162.254.195.87:27020;162.254.195.71:27022;162.254.195.82:27018;162.254.195.71:27017;162.254.195.71:27020;162.254.195.67:27018"
				"CMVer"		"-1299633720"
				"NCTF"		"0"
			}
		}
	}
}
```

* `[SrcDS Root]` refers to the install directory of Source 2007 Dedicated Server; it does not refer to the `gesource` or `steamcmd` folders.
* It is important to quit the server before modifying the file, because when quitting, the server will write out an empty/invalid config.vdf.

## Firewall Restrictions

While there is not a complete and reliable list of Steam endpoints, it is known that you will need to allow the following ports for outbound connections:

TCP ports 80 and 443

TCP ports 27015-27030

UDP ports 27015-27030

## Connection Log

It has anecdotally not been very helpful, but a last-resort option is to check the connection log for your server, which is located at `[SrcDS Root]/logs/connection_log_[port].txt`
