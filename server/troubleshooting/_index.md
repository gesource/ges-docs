---
title: "Server Troubleshooting"
weight: 900
---

Welcome to the troubleshooting section for GoldenEye: Source servers.

If you're a _player_ having trouble with the game itself, see our [player troubleshooting guide](/guides/troubleshooting).

Choose from a topic below:

{{% children  %}}