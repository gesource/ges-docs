---
title: "Windows: Server FPS/Tickrate"
---

By default you may witness server FPS capped to 64 FPS, as well as a fluctuating update rate (as observed by `net_graph` on the client.)

The solution is to enable the Windows high-resolution timer, which can be achieved by tools such as [HiResTimer](https://www.brainless.us/downloadcategory/9/fps-booster-service).

This is a cleaner solution than the traditional `srcdsfpsboost.exe`, as it installs as a service rather than requiring someone to be logged in.