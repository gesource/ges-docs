---
title: "Basic Server Commands"
weight: 500
---

Here are some useful console commands for running a GoldenEye: Source server.

Note that these commands must be run _on the server_. If you are not on the server console, you will need to set `rcon_password` on your client to the same password that is on the server and then prepend each of these commands with `rcon`.

For example, with these commands in the Developer Console:

```
// You only need to run this once
rcon_password MySuperSecurePassword

// Now you can execute commands on the server with rcon
rcon changelevel ge_archives
rcon ge_gameplay mwgg
```

An alternative to the built-in rcon is `sm_rcon` included with Sourcemod, which lets you control rcon access based on SteamID. This is generally recommended, but is beyond the scope of this article aimed at beginners.

---

### Gameplay

| Command | Description |
|:--------|:------------|
|`maps *` |Get a list of maps on the server |
|`changelevel <string>` | Ends the current round, changes to the specified level |
|`mp_timelimit <int>`| Match (map) time limit, in minutes |
|`ge_roundtime <int>`| Round time, in seconds |
|`ge_endround`| Ends the current round |
|`ge_endmatch`| Ends the current match (changes level) |
|`ge_gameplaylist`| Get a list of gameplay scenarios (game modes) on the server |
|`ge_gameplay <string>`| Change to the specified gameplay scenario (game mode) |
|`ge_weaponsetlist`| Get a list of weapon sets on the server |
|`ge_weaponset <string>`| Set the weaponset (takes effect on next round){{<br>}}Value is the weaponset identity (from `ge_weaponsetlist`) or `random_loadout` |

### Fun

| Command | Description |
|:--------|:------------|
| `ge_paintball <int>` | 1 = Enable paintball mode{{<br>}}0 = Disable |


### Moderation

If players get a little unruly, here's what you can do:

| Command | Description |
|:--------|:------------|
| `status`  | Gets a list of players, user (connection) IDs, unique (Steam) IDs, and IP addresses |
| `kick <string>` | Kick the player of the given user name |
| `kickid <int>` | Kick the player of the given ID (first column of `status`) |
| `banid <minutes> <userid \| uniqueid>` | Ban the given player connection ID or Steam ID |
| `banip <minutes> <ip address>` | Ban the given IP address |


## Next Steps

If you've made it this far, you've set up a working GoldenEye: Source server. Congratulations!

See what else you can do by browsing the rest of our [server](/server) documentation.