---
title: "Accessing Your Server From the Internet"
weight: 300
---

To access your server over the Internet, you need to allow the following through any firewalls. If you're behind a NAT router (if you're not sure, the answer is probably yes), you also need to forward these ports to the device hosting your server.

* 27015 (TCP)
* 27015 (UDP)

You can change the port a server uses by adding the `-port` parameter to your launch command. For example:

```
srcds.exe -port 27016 -game [...]
```

{{% notice style="info" %}}
The TCP port is only used for RCON (Remote CONtrol -- that is, remote server administration).

If you do not use the built-in RCON, you can achieve better security by allowing only the UDP port.
{{% /notice %}}

{{% notice style="info" %}}
Some residential ISPs utilize [CGNAT](https://en.wikipedia.org/wiki/Carrier-grade_NAT). If that is the case, you cannot host a public server.
{{% /notice %}}

## Verifying Connectivity

There are two ways to verify others can connect to your game server:

* Try connecting from another Internet connection
* Use the [Server Test Tool](https://forums.geshl2.com/index.php/topic,9079.0.html) from the device hosting the server

## Next Steps

With connectivity out of the way, you can start:

[Customizing Your Server](../customize)