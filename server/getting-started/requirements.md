---
title: "System Requirements"
weight: 100
---

The system requirements of a SrcDS server are pretty low nowadays:

* CPU: Basically anything will do nowadays. When it comes to hosting multiple servers from a single machine, we recommend limiting the load to one SrcDS instance (server) per core. Higher specced machines you may be able to get away with more than one SrcDS instance per core. \
 \
We recommend starting off with at least 2 cores for performance reasons.

* RAM: You're looking at less than 512MB of RAM usage per SrcDS instance.
* Network:
  * Static IP: If you're hosting a public server, we recommend a static IP as it will make it easier for returning players to find your server.
  * Speed: At least 1Mbps download, 8Mbps upload (16-slot server).
  * Latency: Lower is better! We recommend less than 100ms between the client and the server for an optimal experience.

## Next Steps:

Install your GoldenEye: Source server:

* [Linux](../install/linux)
* [Windows](../install/windows)