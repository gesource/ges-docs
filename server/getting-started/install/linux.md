---
title: "Linux Server Install"
---

Running the server natively on Linux has proven to be highly problematic, but luckily it works flawlessly using Wine. This guide was written for Ubuntu Server 18.04, but you could likely adapt it to other distributions as well.

In short, the installation process is:

* Install required packages:
  * Wine
  * Xvfb (X virtual framebuffer)
  * SteamCMD (Linux version)
  * Download the Windows version of Source 2007 Dedicated Server
  * Download GE:S
  * Create service files to run the server automatically

It is recommended that you create a user for the game and X server to run as – do not run as root. You can name the user whatever you like, but in this guide we use the example `geserver`.

Add the required repositories and install the packages:

```
sudo add-apt-repository multiverse 
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install wine-stable xvfb steamcmd
```

Create the server folder tree and set permissions:

```
sudo mkdir -p /servers/geserver
sudo chown -R geserver:geserver /servers/ 
cd /servers/geserver
```

Start SteamCMD:

```
steamcmd
```

It will update and eventually you'll land at the Steam> prompt. Now, we'll download the Windows version of the Source 2007 Dedicated Server. Note that it is important to specify the full path to the install directory here.

```
@sSteamCmdForcePlatformType windows
logon anonymous
force_install_dir /servers/geserver
app_update 310
quit
```

Grab the Server install archive from the Server Downloads page. Then, extract the `gesource` folder to `/servers/geserver`.

With the files in place, it's time to make the systemd service. First, we'll create a service file for Xvfb. This will allow the X server to run on startup and provide a display for Wine to use.

Create and open the file `/etc/systemd/system/xvfb.service`:

```
sudo nano /etc/systemd/system/xvfb.service
```

Now, paste the following:

```
[Unit]
Description=X Virtual Frame Buffer Service
After=network.target

[Service]
User=geserver
ExecStart=/usr/bin/Xvfb :99 -screen 0 16x16x8

[Install]
WantedBy=multi-user.target
```

Press `Ctrl-O` to save and then `Ctrl-X` to exit. Type the following to enable the service at startup:

```
sudo systemctl enable xvfb
```

Start the service:

```
sudo systemctl start xvfb
```

If all goes well, the virtual X display is ready and we can create the service file for the GE:S server.

Create and open the file `/etc/systemd/system/geserver.service`:

```
sudo nano /etc/systemd/system/geserver.service
```

Paste the following:

```
[Unit]
Description=GoldenEye: Source server
After=xvfb.service

[Service]
Type=simple
User=geserver
Environment="DISPLAY=:99"
WorkingDirectory=/servers/geserver
ExecStart=/usr/bin/wine srcds.exe -console -game gesource +map ge_archives
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target
```

Save and close the file. Enable and start the service:

```
sudo systemctl enable geserver
sudo systemctl start geserver
```

The game server will now run on startup of your Linux server. As you need to configure your server, you can use the following commands to start, stop, and restart your server:

| Action |	Command |
|:-------|:---------|
| Start  | sudo systemctl start geserver |
| Stop 	 | sudo systemctl stop geserver  |
| Restart a running server | sudo systemctl restart geserver |

The game server should now be running and ready for further configuration. See our Server Troubleshooting section if you run into any issues.

## Next Steps

Your server may be running, but only people on your local network can connect to it! To learn how to make your server accessible globally, see:

[Accessing Your Server From the Internet](../../accessing-your-server)