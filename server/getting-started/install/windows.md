---
title: "Windows Server Install"
weight: 200
---

This article contains instructions for installing a GoldenEye: Source dedicated server under Windows.

## File Paths in This Guide

For this guide, we will install things to certain directories as examples. However, provided that you modify any references to a path, everything we download can be put wherever you like.

These are the installation directories we will use in this guide:

SteamCMD: `C:\steamcmd`

Source 2007 Dedicated Server: `C:\servers\GEServer`

GoldenEye: Source: `C:\servers\GEServer\gesource`

It is the writer's preference to install the Source 2007 DS and GE:S under the same root folder (GEServer in this case) and keep one copy of the Source 2007 DS per GE:S server, as this is sometimes easier, particularly when using game server control panels such as UGCC.

However, this is not required. You can optionally install one shared copy of Source 2007 DS to save space. This guide will assume you want to go with the layout specified by the file paths above, in which each GE:S server has its own copy of the Source 2007 DS.

## Prerequisites

Download and install the 32-bit [Visual Studio C++ 2012 redistributable](https://www.microsoft.com/en-us/download/details.aspx?id=30679). Make sure the file you download is named `vcredist_x86.exe`. 

## Source 2007 Dedicated Server

To download Source 2007 Dedicated Server, you need SteamCMD, the command-line version of Steam. You can download SteamCMD from the following URL: https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip

Extract the contents of the archive you downloaded to `C:\steamcmd` and then run the newly-extracted `C:\steamcmd\steamcmd.exe`.

After you have downloaded and run SteamCMD, it will update and eventually you will land at the `Steam>` prompt.

When at the `Steam>` prompt, log onto Steam anonymously by typing:

```
logon anonymous
```

Set the target install directory:

```
force_install_dir "C:\servers\GEServer"
```

Install Source 2007 Dedicated server:

```
app_update 310
```

Once you're all done, safely exit Steam using `quit`. You now have Source 2007 Dedicated Server installed. You may need to repeat this process if Valve releases an update for Source 2007.

{{% notice style="tip" %}}
You can automate all of the above at the command line. Simply run the executable, appending the commands in the order you want them to run in, with a plus sign (+) before each command. This is useful for automating future installations and updates of the server. For example:

```
steamcmd +logon anonymous +force_install_dir "C:\servers\GEServer" +app_update 310 validate +quit
```
{{% /notice %}}

## GoldenEye: Source

Grab the Server install archive from the [Server Downloads](https://www.geshl2.com/download) page. Then, extract the `gesource` folder to `C:\servers\GEServer\gesource`.

With the files in place, you are now ready to run the server.

Using Notepad or another text editor, create the file `C:\servers\GEServer\start.bat`. Paste the following into the file:

`srcds.exe -console -game gesource +maxplayers 16 +map ge_archives`

Run `start.bat` and if all goes well, your server is now running. If you are on the same LAN as the game server, you should see the server pop up under the “LAN” tab of the server browser.

## Run as a Service (Optional)

The above instructions get the server running _interactively_ – that is, running on-screen with a user logged in.

To run as a Windows service, follow the instructions below:

_todo_

## Next Steps

Your server may be running, but only people on your local network can connect to it! To learn how to make your server accessible globally, see:

[Accessing Your Server From the Internet](../../accessing-your-server)