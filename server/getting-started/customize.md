---
title: "Customizing Your Server"
weight: 400
---

This will guide you through some of the options for customizing your server. While it is not exhaustive, it does have the basics that you'll want to start with.

## Server Settings

### Message of the Day

The Message of the Day, or MOTD, is the message that people see on the team select window when they first join the server. The MOTD is stored in `gesource/motd.txt`

You can put your own (simple) HTML in here, or link to an external webpage. For more details, see [this page](/server/configure/motd).

### Server-Wide Config

In your `gesource` server folder, open the file `cfg/server.cfg` using a text editor.

This is the server-wide config file. It is executed each time a level is loaded.

There will be a lot of items in this config file, and as you build your server, you may even add some of your own! These are the most pertinent ones to look our for:

* `sv_region`: Set to one of the integers as specified [here](https://developer.valvesoftware.com/wiki/Sv_region)
* `hostname`: This is the name of your server as it appears in the server browser and in-game
* `sv_password`: If set, players will be prompted for a password to join your server.
* `rcon_password`: If set, you can control the server remotely using this password. Don't set this if you aren't using it. If you are using it, make sure it's a very good password!

## Gameplay-Specific Config

You may notice some default config files like `ltk.cfg` or `arsenal.cfg`. These are config files that are executed when the game mode loads (after `server.cfg`).

You can use these config files to make gameplay-specific changes to your server. A common one is to enable `ge_paintball` on LTK. Just make sure to add a line in `server.cfg` to revert these changes on the next map!

Learn more about config files [here](/server/config/reference.

## Next Steps

You've made some light customizations to your server. Now, learn about:

[Basic Server Commands](../basic-commands)