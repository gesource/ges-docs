---
title: "Adding Weapon Sets"
---

GE:S uses loadout script files to specify the composition of weaponsets and what gamemodes they should appear in.

Loadout scripts are stored in `gesource/scripts/loadouts`. Files that specify which loadouts a given gameplay should use start with `gameplay_` and files that specify loadout compositions start with `weapon_sets_`

## Making Custom Weaponsets

Open up `weapon_sets_custom.txt` in `gesource/scripts/loadouts` and follow the guide at the top of the file.

You can also make new files that start with `weapon_sets_custom_` if you would like to keep things more organized.

## Setting up Gameplay Weaponset Rotation

The file that controls the weaponsets in rotation for a given gameplay will be called `gameplay_GAMEPLAYNAME`. For example, arsenal is `gameplay_arsenal`.

The three columns in this file are the weaponset name, weight, and grouping.

The name of the weaponset will be used to pull the weaponset from the existing `weapon_sets_*` files, and the weight and group will override whatever is specified for that weaponset in those files.

If a gamemode does not have a `gameplay_*` file, it will just use the default loadout settings specified for each loadout in the `weapon_sets_*` files.
