---
title: "Adding Gameplays"
---

GoldenEye: Source fully supports custom game modes, and it's easy to add your own, too!

Game modes are located in `gesource/python/ges/GamePlay/`. After placing a game mode file there, it can be loaded using the command `ge_gameplay <gameplay_name>`. To add the game mode to your gameplay cycle, see \[TODO\].