---
title: "Adding Levels"
---

You can add custom maps to your server. A custom map includes its own layout and can also include any of its own assets, such as additional sounds, models, and textures. To add a map to your server:

## Copy the Map Files to Your Server

This is the simple part. Usually, the mapper will have packaged up their map in a way where the files they provided can be safely merged with your GE:S folder.

## Write a Download List

### The Easy Way (Using SourceMod Plugins)

If your server runs SourceMod, you can use the SM File/Folder Downloader and Precacher plugin, which is more flexible and easier to use than the built-in download list system. It is not recommended to use the precache functionality, however.

### The Hard Way (Using the Inbuilt System)

Download Lists are text files with the .res file extension. They list all the files (except for the .bsp file) that are required for a map. They should be located inside the maps folder, and bear the same name as the relevant map file.

In this example, we will write a .res file for a map named ge_map. The .res file will be located at maps/ge_map.res.

```
"resources"
{
"materials/VGUI/loadingscreens/ge_map.vmt" "file"
"materials/VGUI/loadingscreens/ge_map.vtf" "file"
"materials/VGUI/loadingscreens/ge_map_widescreen.vmt" "file"
"materials/VGUI/loadingscreens/ge_map_widescreen.vtf" "file"
"scripts/soundscapes_ge_map.txt" "file"
"scripts/music/level_music_ge_map.txt" "file"
"sound/music/my_levels_music.mp3" "file"
}
```

## Add it to Your Map Cycle

If you use a map cycle, add it to that.

To add your map to the server's map cycle, list the map file (excluding the file extension) on a new line in your map cycle file. By default, the map cycle file is mapcycle.txt. If you have any plugins that modify the map selection, then the procedure for your server may be different.

## Create a Map Script

If the map did not come with its own script file, you will need to make one.

Map scripts are located in `scripts/maps`, and use the name of the map as the file name (with txt as the extension, instead of bsp).

## Add it to Your Fast Download Server

Upload to your Fast Download server. If you don't know what that is, see [Fast Download Server](/server/configure/fastdl).
