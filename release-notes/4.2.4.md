---
title: "v4.2.4 Release Notes"
date: "2013-11-10"
---

Released: {{<article_date>}}

---

We've fixed all submitted issues associated with crosshairs and aiming.
 
* Aiming being sluggish and slow
* Dying while zoomed would stay zoomed while blood screen plays.
* Crosshairs correctly turns off now while reloading.
* Changing your resolution while playing now correctly re-centers the crosshairs.
* When zooming in/out with the mouse wheel will no longer invoke the weapon selection panel. 

Bugs fixed that were associated with gameplay:

* Now when spectating a player while dead or in spectator mode their health/armor shows correctly.
* Payers will no longer get stuck into wall/props when spawning.
* Projectiles(grenades,rockets,etc) now correctly pass through teammates.
* End of the match will now properly show the round scores, and not skip them for final scores. 

Game modes also have seen a couple bug fixes:

* Various capture area code fixes/enhancements. Therefore, uplink will now have properly sized capture areas.
* gameplay_capturethekey.txt was added back so server admins can now use that.

New tracks in this update include:

* Archives
* Caverns
* Frigate
* And two new surface versions
