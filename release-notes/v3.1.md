---
title: "Beta 3.1 Release Notes"
date: "2009-03-14"
---

Released: {{<article_date>}}

---

## Server Changes

* Added: cvar ge_gameplay_mode
  * 0: Same GamePlay as match before
  * 1: Random GamePlay from gameplaycycle.txt
  * 2: Ordered GamePlay from gameplaycycle.txt
* Added: cvar ge_gp_cyclefile which selects the game play cycle to use (default gameplaycycle.txt)
* Added: cvar ge_autoteam Which does this: If greater than zero it turns teamplay on when player count is greater than the value and turns teamplay off when lower during round end.
* Added: Weapon loadout random_loadout which selects a different approved loadout each round
* Added: ConVars to certain game modes
* Added: Over 30 LUA Callbacks and Functions. Refer to Lua Reference.
* Added: “official” tag to gamemode event message
* Added: HLStatsX server messages player_hurt and player_shoot
* Added: Ability to exclude any weaponset from the random loadout selection by simply adding “allowrandom” “0” in the definition of the loadout
* Changed: ge_weaponset defaults to “random_loadout”
* Changed: mp_timelimit defaults to 20 mins
* Changed: Server config file (defined by cvar servercfgfile) gets executed on every gameplay load
* Changed: sv_tags auto settings…
  * ge_weaponset
  * sv_cheats
  * ge_gameplay_random
  * ge_autoteam
* Fixed: The map cycle actually loads the correct map next even when a forced map change occurs, it will never load the same map in the row as long as there are multiple maps defined in mapcycle.txt
* Fixed: mp_timelimit 0 and ge_roundtime 0, they function as they should to disable the time limits.
* Fixed: Optimized the player's transmittal state for the radar, the prior implementation was causing WAY too much net traffic for 10+ players.
* Fixed: Coloring of radar blips through the radar resource
* Fixed: Server crash when trying to change to bad character name.
* Fixed: Radar exploits in the LUA callbacks 

## Client / HUD / VGUI Changes

* Added: Spanish translations
* Added: Help to gamemodes (type !help in the chat)
* Added: Show the current loadout and its weapons by typing !loadout into the chat
* Removed: +zoom, zoom_toggle, +speed
* Removed: HL2 Impulse Commands (51/52/etc)
* Removed: Black screen on fall death
* Removed: HL2 MOTD window
* Removed: Shadow from the round timer font
* Changed: Doubled the friendly id hint distance
* Changed: When you are spectator, you now show *SPEC* instead of *KIA*
* Changed: Moved the death music client side and is toggleable with the “Disable Special Cue Music” advanced multiplayer option
* Changed: The chat box isn't repositioned until after you finish typing on round end/restart
* Changed: Death Message background is always hidden now (was showing up as spectator sometimes)
* Fixed: Spectator hat bug
* Fixed: Spectator map change glitch
* Fixed: Show char select menu after round report if it was open before (fix for when you join some servers)
* Fixed: When rounds are disabled, only the match scores show. When the final round happens, the round scores show, then the match scores show with score tally disabled to prevent duplicate scoring of the last round. 
* Fixed: Scaling issue on the scoreboard at lower resolutions
* Fixed: Player names showing up randomly on the scoreboard in teamplay when more then 10 people were on the team
* Fixed: Awards will now show on the round report in proper sequence (no -NONE- sprinkled around)
* Fixed: Crash with ragdolls in a rare case the player's entity is invalid when the ragdoll is created

## Gameplay Changes

* Added: Ability to walk through teammates
* Removed: Duck Jumping is disabled
* Changed: Proximity Mine sound plays at intervals if you are within the warning distance instead of blowing up right when you hear the noise
* Changed: When you first pickup a weapon it will automatically load the clip with as much ammo as you have for that weapon (instead of the default clip size, usually 10 rounds)
* Changed: Spectators that scored during the MATCH show up on the MATCH REPORT
* Changed: Impulse 101 now gives armor
* Changed: Some weapon damage values for balance. Reduced Shotgun recoil.
* Changed: Increased the jumping height to 45 units so that you can clear railings and mount crates
* Changed: Proxy mines now be exploded if thrown objects pass them within their detonation range. Thrown objects include:
  * TKnifes
  * Grenades
  * Any mine
* Fixed: Spawning system is completely revamped and spawn kills will NEVER occur. Players are placed on a spawn queue if no spots are available and will spawn in as soon as one opens up.
* Fixed: TKnifes impacting the hitboxes of objects, now they collide with the player better and also don't send crates and weapons flying when they clearly don't hit them
* Fixed: Spectators don't detonate or trigger proximity mines
* Fixed: Thrown grenade blocking your ability to walk
* Fixed: Special Weapon (Golden Gun) not spawning in all available slots in the map

## Achievements Changes

* Added: 13 new achievements.
* Changed: Reduced the amount of kills required for the KLOBB achievements
* Changed: Increased the amount of kills required for Secret Agent and 00 Agent weapon achievements
* Changed: When playing LTK, all kill based achievements count 1 for every 3 kills
* Fixed: All character based achievements ignore the case of the ident
* Fixed: Prevented round based achievements from being awarded if the round didn't count (ex. warmup time)
* Fixed: Certain achievements won't be given for suiciding
* Fixed: Event based achievements (round end, change character, etc.) were failing after a map change.

## Map Changes

* ge_control updates
  * Added more player spawns
  * Added/Tweaked weapons spawns
  * Prevented control music from restarting when exiting elevators (continues playing)
#
* ge_complex updates
  * Added more player spawns
  * Minor layout tweak
#
* ge_complex_classic updates
  * Added more player spawns
  * Made grates passable to bullets like the original

