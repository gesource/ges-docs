---
title: "Release Notes"
weight: 200
---

This section contains the release notes for each version of GoldenEye: Source. Pick the release notes you want to read from the list below.

{{% children  %}}
