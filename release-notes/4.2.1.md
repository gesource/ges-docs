---
title: "v4.2.1 Release Notes"
date: "2012-08-09"
---

Released: {{<article_date>}}

---

* Added: Added extra api disconnect call to prevent hanging hl2.exe
* Added: Added convar cl_ge_nohitsound to allow clients to disable the hit feedback sound
* Removed: Removed bot threshold default to prevent servers from blindly locking themselves out
* Changed: Fixed round/match end determinations and timings
* Changed: Fixed typos and errors in CTK overtime
* Changed: Added some more checks for bots to not pursue weapons and armor in certain modes
* Changed: Reduced volume of hint (but not popup message) notice sound
* Changed: Localized create server menu
* Changed: Added more localization for the team menu
* Changed: Fixed bots wearing the incorrect hats after team change
* Changed: Fixed bots getting stuck in doors occasionally
* Changed: Fixed caves ai node graph
* Changed: Fixed armor sound looping
* Changed: Attempt at a fix to spawners filling when recently used
* Changed: Fix LD flags going haywire when one was added or removed
* Changed: Fixed spawning issues when a player joins a server
* Changed: Made spawning more robust to spawn points being filled and acting as an observer
* Changed: Reordered OnPlayerDisconnect and the corresponding hook calls to ensure the gameplay doesn't try to use functions after cleanup
* Changed: GEPlayer.ToGEMPPlayer(..) will not convert an entity AND a UID to a player
* Changed: Round end music will play even for players not spawned in yet
* Changed: Fixed SM not being able to change the level 