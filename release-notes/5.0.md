---
title: "v5.0 Release Notes"
date: "2016-08-12"
---

Released: {{<article_date>}}

---

## New Features

4 new maps have been added:

* Added: Archives Classic
* Added: Bunker
* Added: Dam
* Added: Depot

2 new game modes have been added:

* Added: A View to a Kill:
  * You are scored on time spent alive
  * Dying resets current time to 0
  * Killing a player adds 50% of their currently accumulated “time alive” to yours
  * Slapper kills will reward you with 75% of their currently accumulated time
  * Current time leaders are highlighted with red markers

* Added: Gun Trade:
  * Each player is randomly assigned a weapon at the start of the match
  * Killing, or being killed by another player will swap weapons between you and that player
  * Slapper kills remove the victims weapon from circulation and assigns a new weapon that is not currently in circulation
  * The player with the most kills at the end of the round wins

2 new playable characters have been added:

* Added: Russian Infantry
* Added: Russian Soldier

Other Features:

* Added: 25 new music tracks
* Added: 40 new achievements
* Added: Added Community Contributor rank which comes with a scoreboard tag and Klobb skin

## Level Design Updates

The following maps have had significant layout changes and/or have been entirely remade to improve game balance and overall performance:

* Remade: Aztec
* Remade: Caverns
* Remade: Caves
* Remade: Complex Classic
* Remade: Facility Classic
* Remade: Basement Classic
* Remade: Library Classic
* Remade: Stack Classic
* Remade: Runway
* Remade: Temple Classic
* Modified: Basement
* Modified: Control
* Modified: Cradle
* Modified: Egyptian
* Modified: Facility
* Modified: Facility Backzone
* Modified: Silo

The following maps and their respective assets have received visual updates:

* Updated: Archives
* Updated: Bunker Classic
* Updated: Casino

## Game Mode Updates

* Modified: Arsenal:
  * each round now consists of an 8-weapon set (down from 16)
  * each weapon requires 2 kills to advance to the next

* Remade: Capture the Flag (Formerly Capture the Key):
  * Dropped flags are now returned immediately on contact
  * Overtime only triggers if the round ends in a tie

* Removed: Live and Let Die

## Art & Sound Updates

* Added: The following weapons have received new models and muzzle flash particle effects:
  * Slappers
  * Hunting Knife
  * Throwing Knife
  * PP7/Silenced PP7
  * DD44
  * Klobb
  * KF7
  * ZMG
  * D5K/Silenced D5K
  * Phantom
  * RC-P90
  * Shotgun
  * Automatic Shotgun
  * Sniper Rifle
  * Cougar Magnum
  * Golden Gun
  * Silver PP7
  * Gold PP7
  * Grenade Launcher
  * Rocket Launcher
  * Hand Grenade
  * Timed Mine
  * Remote Mine
  * Proximity Mine
  * Watch Detonator
* Added: Support for special event weapon skins
* Added: Weapons with visible shells (Shotgun, Automatic Shotgun, Grenade Launcher) will now properly reflect the ammo count if reserve ammo is less than the reload capacity
* Added: Weapon fire attenuation added to better tell what direction weapon fire is coming from
* Modified: Weapon skin system overhauled, now supports 4 distinct skins per weapon that can be picked up by other players
* Modified: Rocket launcher projectile is now a part of the viewmodel and transistions to world model when fired
* Modified: Props will tint to a darker color when they're damaged, unless specified otherwise by the mapper
* Remade: New weapon firing and explosion sounds.
* Remade: New grenade and throwing knife impact sounds.
* Remade: New Ammo and Weapon HUD Icons
* Remade: New bullet impact decals
* Remade: New bullet impact particle effects
* Remade: New blood decals
* Remade: New sprays
* Removed: Old sprays removed

## Gameplay Updates

* Added: Strafe running now gives a speed increase up to 1.4x base running speed
* Added: Weapons will now dry-fire if trigger is held after emptying the magazine
* Added: Damaging a player before they kill themselves now properly awards the kill to the attacker
* Added: Falling short distances no longer deal damage but rather give you a temporary movespeed penalty
* Added: Context-sensitive hit sounds have been added for head, body and limb shots
* Added: Mines can now be stacked on top of each other
* Added: Primed grenades will now drop and detonate if the player is killed before they are thrown
* Added: Hit sounds added for damaging players with an explosion
* Added: A new kill confirmation sound can be toggled in the multiplayer-advanced tab
* Added: Added proper acceleration and deceleration values to doors and elevators
* Added: Players can now drop multiple weapons on death, as specified by ge_weapondroplimit. By default they drop their active & strongest unused weapon.
* Modified: Random Weapon Set now creates sets with coherent weapon ordering
* Modified: Proximity mines are now sensitive to velocity. Crouching/walking will no longer set them off
* Modified: Gamemodes can now control spawn invulnerability duration and if it can be broken by picking up weapons/armor
* Modified: Spawn invulnerability now provides radar invisibility as well, to allow players to escape nearby aggressors
* Modified: Damage code redone, along with explosion blast code so that both are much more consistent.
* Modified: Weapon autoswitch will now avoid coming into effect if players are currently firing their weapon or have an explosive out.
* Modified: No more autoswitch to timed mines.
* Modified: Rockets and grenade launcher rounds are instant kills on a direct hit, and display as such in the killfeed
* Modified: Shotgun spread is now only psuedorandom, preventing unfair spreads
* Modified: Weapon spread now follows a gaussian destribution, with values that can be changed per-weapon. Shots tend towards the center of the crosshair and accuracy degregredation is sensible.
* Modified: Invulnerability is now a damage cap over time instead of damage protection after each hit
* Modified: Accuracy and recoil mechanics
* Modified: Weapon damage values
* Modified: Adjusted weapon kickback to be much more subtle
* Modified: Weapon penetration special rules: Can no longer shoot through the entirety of maps. Door penetration unchanged.
* Modified: Increased required fall distance to trigger player damage
* Modified: Sniper Rifle zoom is retained between usage
* Modified: Sniper Rifle default zoom reduced to 50% down from 100%
* Modified: Spawning system greatly improved. You should no longer spawn directly beside an opponent
* Modified: Hunting knife removed from spawn weapons
* Modified: Slapper damage increased
* Modified: Hunting knife damage increased
* Modified: Melee hit tracing significantly improved. It is now easier to hit players with slappers and hunting knives
* Modified: Throwing knives now find their target much easier
* Modified: Movement speed increased for running and crouching
* Modified: Armor vests respawn if knocked too far away from their spawn point
* Modified: Switching teams now always forces a suicide
* Modified: Explosions now only shake screen if nearly inside of them
* Modified: Armor will no longer spawn as quickly if 2 players are fighting over it
* Modified: New crouch jump animation to prevent large increase in apparent jump height
* Modified: Round timer now flashes red/green when time is removed/added to it
* Modified: Aimmode reduces weapon kickback
* Modified: Round end conditions are more robust
* Modified: Weapon spawners can now spawn their normal weapon after their override token has been picked up, eg: Golden Gun
* Modified: props now take 1.5 times the damage to compensate for weapon damage adjustments
* Modified: Reloads are audible to all players
* Modified: Moved grenades collision group so they don't collide with players
* Modified: Moved rockets collision group so they properly collide with players
* Modified: Grenade/rocket projectiles no longer collide with weapon triggers or tokens
* Modified: Grenade projectiles now have a team based collision group in team mode, so they no longer collide with teammates
* Modified: Golden PP7 and Silver PP7 now use golden gun and magnum ammo, respectively
* Modified: Picked up weapons now give 30 bullets instead of 10 (Some exceptions such as the Golden Gun)
* Modified: Throwing knives now only damage players
* Modified: Pushforce from weapons redone. High damage hits push players further and low damage hits have no effect at all.
* Fixed: Throwing knife and hunting knife no longer switch to eachother with right click
* Removed: Cooldown period between jumping has been removed

## Console / Client / HUD / VGUI

* Added: Impulse 28 cheat, which gives you a unique radar color based on your steamID
* Added: ge_infiniteammo added, along with gameplay specific enabling of this functionality
* Added: Added map-specific death messages to the kill feed
* Added: Added console variable to hide dev tags and weapon skins
* Added: Added “ge_nextsong” to tell the music manager to skip the current track
* Added: Kills awarded to an attacker as a result of player suicide are displayed in the kill feed
* Added: Added weapon help popout, which is accessed with F. Displays all the relevant stats of the current weapon.
* Added: Added cl_ge_show_ammocount to disable ammo display
* Added: Added cl_ge_show_pickuphistory to disable pickup icons
* Added: Added console variable, ge_print_map_selection_weights, which prints all the viable maps and their weights
* Modified: Main menu background updated
* Modified: Changed some achievement names, “You Should Feel Honored” is now “You Should Feel Suave” and the criteria is different
* Modified: Fast weapon switch now has the option to show the player's entire inventory when switching
* Modified: Right-aligned weapon icons in weaponset popout help
* Modified: teamscore HUD box only displays round score now
* Modified: Rocket and grenade fuse times adjust based on phys_timescale
* Fixed: Fixed gameplay popout help
* Fixed: Fixed giveweapon function so it can't spawn weapons inside the floor and fail to give them to the player
* Removed: “Custom Map” loading screen removed

## Bot Fixes

* Fixed: Bots will always take damage from the same triggers players do
* Fixed: Bots use proper hull size now, which will help them avoid getting stuck
* Fixed: Bots have had a rework of their behavior - will now hunt weapons and players reliably
* Fixed: Bots no longer get stuck frequently
* Fixed: Bots now wait the correct time for doors to open
* Fixed: Bots are more aware of nearby enemies and items
* Modified: Heavy rework of map bot networks for more intelligent bot behavior

## General Fixes & Performance

* Fixed: Fixed an issue where you could get stuck by “X”ing out of the character select menu
* Fixed: Fixed an issue where grenade launcher rounds would not bounce
* Fixed: Fixed various movement exploits such as wall strafing and wiggle walking
* Fixed: Fixed crouch jumping causing hitboxes to be inaccurate
* Fixed: Fixed desyncing between doors and child objects such as windows
* Fixed: Fixed “The Man Who Cannot Die” achievement being granted to players who had more than 10 kills on match end
* Fixed: fixed teamplay variable getting out of synch which would mess up lag compensation
* Fixed: fixed weapon-switch bug where you couldn't switch backwards between weapons in group one if you had to jump to the other side of the group
* Fixed: Fixed rockets colliding with capture areas
* Fixed: Fixed timer animation bugs
* Fixed: Fixed aimmode bug where players occasionally would lose access to their crosshair
* Fixed: Roundtimer now displays the correct time in all cases
* Fixed: Roundtimer now responds much better to being changed mid-round
* Fixed: Lasers can no longer be deleted without pickup if player doesn't have one
* Fixed: Fixed bullet tracers and muzzle flashes to be more consistent
* Fixed: Hitscan weapons now refuse to be blocked by lesser damage bodyparts if they can also hit a higher damage bodypart in the same trace
* Fixed: Fixed penetration code ignoring entities penetrated shots ended up inside of
* Fixed: “You can't win” achievement is now consistently obtainable
* Fixed: Items can no longer be picked up through walls
* Fixed: Players will now pick up the entire ammo crate when only picking up some of the ammo in it
* Fixed: Respawnable entities will no longer get blocked by nonsolid entities like precipitation
* Fixed: Player hull expanded by 0.5 units - It is no longer possible to see through walls when close to them on 16:9 resolution
* Fixed: Aimmode functionality adjusted to prevent prediction errors and more seamlessly integrate with gameplay
* Fixed: Fixed After Action Report bug with teamplay mode being displayed on first FFA round after a teamplay round
* Fixed: Fixed After Action Report displaying wrong gameplay at the top
* Performance: Explosions no longer have heatwave or dynamic light effects
* Performance: Weapon muzzle flashes no longer light up static props
* Performance: Precached weapon assets
* Performance: Avoided precaching some unused assets
* Performance: Datacachesize bumped to 128 MiB

## Python

* Added: Added python itemtracker which monitors items currently on the map for use by bots
* Added: Added “OnEnemyTokenTouched” python callback
* Added: canplayerchangeteam python callback given wasForced boolean parameter to let gamemodes know when players did not intentionally make the change
* Added: Added MakeInvisible and MakeVisible python player commands

## Server

* Added: Added ge_addtoroundtime and ge_setcurrentroundtime to give more control to server admins
* Added: Added map manager, a system which uses script files to determine what weight maps should have during selection and what gamemodes can be played on them.
* Added: Added cl_ge_weapon_switchempty, which determines if a player is allowed to pull out weapons with no ammo
* Added: Added round_ranks event for plugins to tell where people placed in the round
* Added: Added map_rec event for game to communicate map selection choices to plugins
* Added: Added ge_gameplay_threshold - a console variable which designates the minimum amount of players before random gamemode selection can occur
* Modified: Capture areas can now be spawned in shortly after round start
* Modified: Scoreboard can be switched to a time based display, for modes like VTAK
* Modified: Create server menu now properly supports random gameplay and puts default weaponsets at the top
* Modified: Only a widescreen loading screen is required for a map now
* Modified: introduced more mapcycle functionality. Rotation will now avoid picking recently played maps, gamemodes, and weaponsets. How far back it looks is adjustable with convars.
* Modified: Changed hardcode ban behavior to just kick instead of ban from server, so it can be reversed later if circumstances require it
* Modified: Round_end event now reports the winner's score
* Modified: Random_loadout selection criteria greatly expanded, will now avoid picking a set it has picked recently and avoids picking the same type of set twice in a row

## Mapping Resources

* Added: Added func_ge_door, a door that can accelerate and change direction mid-motion.
* Added: Added func_ge_brush, a brush entity that can specify a custom collide group
* Added: Added trigger_trap, which is a hurt trigger that can give custom kill credit and kill messages
* Added: Added prop_ge_dynamic, a dynamic prop that can assign itself random skins on round start. Supports custom collision grouping.
* Added: Added ge_logic_gate, a map entity that allows for more simple implementation of complex map logic
* Added: Added ge_door_interp, which allows the visual part of a door to interp on the client instead of the server. Does not work well
* Added: Added functionality to ge_gameplayinfo to allow maps to disable superflous areas at will
* Added: Added ge_point_follower, an entity that will follow a target entity on a delay
* Added: added special “slipnslide” material property functionality to certain material types
* Modified: ge_gameplayinfo can detect more gameplay conditions like teamspawn use, and the specific gamemode in play
* Modified: ge_gameplayinfo can now change the floor height of a map, which is used by the spawning system and radar.
* Modified: spawners now have more outputs for mappers to use
* Modified: ge_gameplayinfo can use GetConnectionCount to measure how many players are currently connected to the server so that the first round can be prepared appropriately
* Modified: tokens can now be defined with skin of -1 to keep previously used skin
* Modified: kill triggers no longer remove weapons from killed players
* Modified: func_ge_brush can be flagged to remove weapons from killed players
* Modified: ge_gameplayinfo no longer fires playercount and roundcount inputs at start of round to allow mappers more control
* Modified: Spectator spawns can now be enabled/disabled by the mapper
* Fixed: func_rebreakable respawns in the correct place when parented to objects
* Fixed: func_door will now respawn the same way as other entities when the map restarts, making complicated entity systems much easier to implement
* Fixed: trigger_push can now lift players off the floor reliably
* Fixed: Fixed ge_debug_checkplayerspawns logic error and added in the ability to check for bot node compatibility

## Networking

* Modified: Increased default network rates
  * Maximum rate allowed by server is 187500 bytes/s (~1.5Mbps) per client
  * Minimum rate allowed by server is 4000 bytes/s (32Kbps) per client
  * Default network data rate used by client is 93750 bytes/s (750Kbps)
* Modified: Changed default server minimum and maximum update/cmd rates and the default client update/cmd rates to 66
* Modified: Reduced interpolation amount from 100ms to ~30.3ms

