# GoldenEye: Source Documentation

This repository hosts the documentation for https://docs.geshl2.com

## Contributing

If you're not a member of the team but have suggestions for how our documentation can be improved, please start a discussion here on GitLab, on the [forums](https://forums.geshl2.com), or on [Discord](https://discordapp.com/invite/MTwT3Ch).

We're also open to outside contributors -- but please discuss with us before sending your pull requests!

## How to Update the Site

### Making Changes

When pushing to the `main` branch of this repository, the changes are automatically built and pushed to the website.

Please allow up to 5 minutes for webpages to be updated, or up to 15 minutes for images (which are cached longer).

### Page Formatting

This site uses Markdown, with some additions/changes. See our reference material for this site [here](README-formatting.md).

### Page Structure

There are a couple different ways webpages can be organized, depending on the usecase.

#### Webpage Has Its Own Folder

In Hugo terminology, this is a [branch bundle](https://gohugo.io/content-management/page-bundles/).

This is where a webpage has its own folder. The folder name dictates the URL of the webpage. The `_index.md` inside the folder contains the markdown used to generate the page.

To illustrate this, see the following directory tree that makes up the Achievements page:

```
<repository_root>
└── achievements
    ├── images # all the images used by the page are here.
    │   │      # This creates the directory on the server: /achievements/images
    │   ├── 004.png
    │   ├── a_salt_rifle_agent.png
    │   ├── a_salt_rifle_secret_agent.png
    │   ├── a_salt_rifle_00_agent.png
    │   └── etc...
    └── _index.md # markdown file used to generate the HTML for this page

```

Pages can also be structured to create sub-pages or categories. This will result in the pages being shown hierarchically in the navigation menu

#### Webpage Is a Standalone Markdown File

A webpage can be a standalone markdown (.md) file. The URL of the page is `/parent folder path/filename` (minus the .md at the end). This format is preferred when the page URL *won't* have any child resources (sub-pages, images, etc) and putting them in their own folder will only generate more work, not reduce clutter.

To illustrate this, see the following directory tree that makes up the Weapons pages:

```
<repository_root>
└── weapons
    ├── images
    │   └── [ weapon image files ]
    ├── special
    │   ├── _index.md # /weapons/special
    │   └── flag.md   # /weapons/special/flag
    ├── _index.md # /weapons
    ├── ar33.md   # /weapons/ar33
    ├── automatic_shotgun.md
    ├── cougar_magnum.md
    ├── [...]
    ├── throwing_knife.md
    ├── timed_mine.md
    └── zmg.md
```

#### File/Resource Locations

As you could see from the example file trees above, we prefer to group things like images with their appropriate branch bundles -- i.e. putting achievement images inside `/achievements/images`, putting weapon images inside `/weapons/images`, etc.

Do **not** create, for example, an `/images` folder. We prefer to keep things organized in a particular way, both due to Hugo's inner workings and because it generally is easier to organize and do future cleanup this way.

## Previewing Your Changes

You may want to preview your changes so you can know what they'll look like and make sure you didn't make any errors.

You can build your own local copy of the site by cloning the [ges-docs-website](https://gitlab.com/gesource/ges-docs-website) repository and putting your changes inside the `content` folder. Then, run `hugo server` inside the website folder (the parent folder of `content`). Hugo will display a link to view the changes (usually `http://localhost:1313/`)

We currently use [Hugo 111.03](https://github.com/gohugoio/hugo/releases/tag/v0.111.3) to build the website.