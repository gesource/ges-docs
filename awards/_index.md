---
title: "Awards"
weight: 100
---

Awards were a very unused feature in GoldenEye 007, so we decided to expand upon the idea of awards and make them a very fun and central addition to GoldenEye: Source.

Awards are based on a point system, and are only given out to the most deserving players. We use a dynamically changing formula based on the number of players in a server to determine if an award should be given.

There are a total of 13 distinct awards in GoldenEye: Source.

---

Legend:

**+** Counts toward the award \
**-** Counts against the award

---

| Icon                               | Name                    |                |
|:-----------------------------------|:------------------------|:---------------|
| ![](images/none.jpeg)              | None                    | **o** No award |
| ![](images/most_professional.jpeg) | Most Professional       | **+** Get Kills {{<br>}} **-** Taking Damage {{<br>}} **-** Be Killed {{<br>}} **-** Attack an unarmed enemy |
| ![](images/most_deadly.jpeg)       | Most Deadly             | **+** Inflict Damage {{<br>}} **+** Get Kills {{<br>}} **+** Attack an armed enemy with slappers |
| ![](images/mostly_harmless.jpeg)   | Mostly Harmless         | **+** Suicide {{<br>}} **+** Be Killed {{<br>}} **+** Taking Damage |
| ![](images/most_honorable.jpeg)    | Most Honorable          | **+** Attack enemy while they are facing you {{<br>}} **+** Attacking an armed enemy with slappers |
| ![](images/most_dishonorable.jpeg) | Most Dishonorable       | **+** Attack enemy while they are facing away from you {{<br>}} **+** Attacking an unarmed enemy  |
| ![](images/lemming.jpeg)           | Lemming Award           | **+** More for Suicide {{<br>}} **+** Taking Damage |
| ![](images/marksmanship.jpeg)      | Marksmanship            | **+** Make a successful hit {{<br>}} **+** More for hitting specific areas {{<br>}} **-** Fire your weapon |
| ![](images/ac10.jpeg)              | AC-10 Award             | **+** More points for filling your Armor less on every pickup |
| ![](images/wheres_the_armor.jpeg)  | Where&#039;s The Armor? | **-** Getting Armor |
| ![](images/longest_inning.jpeg)    | Longest Innings         | **+** Lives lasted a long time |
| ![](images/shortest_inning.jpeg)   | Shortest Innings        | **+** A lot of deaths in a short amount of time |
| ![](images/wheres_the_ammo.jpeg)   | Where&#039;s The Ammo?  | **+** Picking up Ammo |
| ![](images/most_frantic.jpeg)      | Most Frantic            | **+** Every foot of distance moved {{<br>}} **+** Misses with a shot based weapon |