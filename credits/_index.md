---
title: "Credits"
weight: 300
---

## Current Developers

| Name            | Position/Role                              | Developer Since |
|:----------------|:-------------------------------------------|:----------------|
| Entropy-Soldier | Lead Programmer/Game Designer/Project Lead | 4.2             |
| Fourtecks       | Senior Level Designer/Project Lead         | Pre-Alpha       |
| Killer Monkey   | Senior Programmer/Project Lead             | Pre-Alpha       |
| Enzo.Matrix     | Public Relations Lead                      | Co-Creator      |
| Luchador        | Lead Level Designer                        | Beta 3.0        |
| Kraid           | 3D Artist                                  | Beta 1.0        |
| Basstronix      | Lead Sound Designer                        | Pre-Alpha       |
| Goldenzen       | Lead Sound Designer                        | Beta 3.1        |
| soupcan         | Programmer/System Administrator            | 4.2             |
| InvertedShadow  | Programmer                                 | Alpha 1.1       |
| Mangley         | Game Artist                                | 4.2             |
| Adrian          | Level Designer                             | 4.2             |
| Torn            | Level Designer                             | 4.2             |
| Tweaklab        | Music Composer                             | 4.2             |
| JcFerggy        | Quality Assurance                          | 4.2             |
| TZKK²           | Quality Assurance                          | 4.2             |
| Bashe           | Web Designer                               | 4.2             |

## 3rd-Party Contributors

| Name  | Position/Role  | Contribution                                                       |
|:------|:---------------|:-------------------------------------------------------------------|
| Rocko | Music Composer | Responsible for the amazing recreation of Facility Classic's music |
| vaksa | Graphic Artist | Created a few of our older logos                                   |

## Retired Developers

|  Name              |  Position/Role                              |  Developer Tenure      |
|--------------------|---------------------------------------------|------------------------|
|  Audix             |  Music Composer                             |  Beta 1.0 to 4.0       |
|  Spider            |  Lead Artist/Project Lead                   |  Pre-Alpha to 4.0      |
|  SharpSh00tah      |  Level Designer                             |  Alpha 1.1 to 4.1      |
|  Saiz              |  Senior Artist                              |  Alpha 1.1 to Beta 3.0 |
|  ViashinoCutthroat |  Programmer                                 |  Alpha 1.1 to Beta 3.0 |
|  Loafie            |  Lead Level Designer                        |  Pre-Alpha to 4.1      |
|  Lodle             |  Programmer                                 |  Beta 1.0 to 4.2       |
|  Major             |  Community Manager                          |   4.2                  |
|  Baron             |  Art Director/Public Relations/Project Lead |  Pre-Alpha to 4.2      |
|  Fonfa             |  Level Designer                             |  Alpha 1.1 to 4.1      |
|  Semedi            |  Level Designer                             |  Alpha 1.0 to Beta 3.0 |
|  The SSL           |  3D Artist, Animator                        |  Beta 3.1 to 4.2       |
|  DLT               |  2D Artist                                  |  Pre-Alpha to 4.1      |
|  Konrad Beerbaum   |  Lead 3D Artist                             |  Beta 1.0 to 4.1       |
|  Dr. Dean          |  Character Artist                           |  4.2                   |
|  Kinky             |  Animator                                   |  Beta 3.0 to 4.1       |
|  juzcapes          |  Animator                                   |  Beta 3.1 to 4.1       |
|  WakeoftheBunt     |  Lead Quality Assurance/Public Relations    |  Beta 1.0 to 4.2       |

## Former Developers

| Name             | Position/Role     | Developer Tenure          |
|:-----------------|:------------------|:--------------------------|
| Nickster         | Mod Leader        | Creator                   |
| Cloppy           | Public Relations  | Pre-Alpha to Beta 1.0     |
| Sephiroth        | 3D Artist         | Pre-Alpha to Beta 1.0     |
| Framed           | 2D Artist         | Pre-Alpha to Beta 1.0     |
| kant.think.str8  | 3D Artist         | Pre-Alpha to Beta 1.0     |
| Cali             | Level Designer    | Pre-Alpha                 |
| Baddog           | Level Designer    | Pre-Alpha                 |
| raoule           | Level Designer    | Pre-Alpha                 |
| G.Ballblue       | Level Designer    | Pre-Alpha                 |
| misterBlue       | 3D Artist         | Pre-Alpha                 |
| Sir Louis        | Sound Designer    | Pre-Alpha                 |
| Turboracer       | 3D Artist         | Pre-Alpha to Alpha 1.0    |
| Pigsy            | Level Designer    | Pre-Alpha to Alpha 1.0    |
| BlackThorn       | Website/2D Artist | Pre-Alpha to Alpha 1.1    |
| Nudnick          | 3D Artist         | Pre-Alpha to Alpha 1.1    |
| Vader02          | Animator          | Pre-Alpha to Alpha 1.1    |
| Bloodix          | Level Designer    | Pre-Alpha to Alpha 1.1    |
| PerfectH         | Level Designer    | Pre-Alpha to Alpha 1.1    |
| Jeo              | Level Designer    | Pre-Alpha to Alpha 1.1    |
| Dejawolf         | 3D Artist         | Alpha 1.1 to Beta 1.0     |
| Grebe            | Sound Designer    | Alpha 1.1 to Beta 1.0     |
| Kruel            | 3D Artist         | Pre-Alpha to Beta 1.0     |
| Superman         | Programmer        | Beta 1.0 to Beta 1.1      |
| nowhere          | Quality Assurance | Beta 1.0 to Beta 3.0      |
| AdmiralRa        | 3D Artist         | Beta 1.0 to Beta 1.1      |
| KevinJohn3D      | 3D Artist         | Alpha 1.1 to Beta 1.0     |
| JezGreen         | 3D Artist         | Beta 1.0 to Beta 1.1      |
| Zorin Industries | Sound Designer    | Beta 1.0 to Beta 3.0      |
| xenobond         | 3D Artist         | Beta 1.0 to Beta 1.1      |
| ScottL           | Programmer        | Pre-Alpha to Beta 1.1     |
| Mariocatch       | Programmer        | Alpha 1.1 to Beta 1.1     |
| Xanatos          | Public Relations  | Alpha 1.1 to Beta 3.0     |
| Janus            | Web Designer      | Beta 1.0 to Beta 3.0      |
| Armoured Fury    | 3D Artist         | Pre-Alpha 1.1 to Beta 3.0 |
| Sporkfire        | Level Designer    | Beta 1.0 to Beta 4.0      |
| Kickflip         | 3D Artist         | Alpha 1.1 to Beta 3.0     |
| cubedude89       | 3D Artist         | Beta 3.0 to Beta 4.0      |
| ExtraNoise       | 3D Artist         | Beta 3.0 to Beta 4.0      |
| SLX              | 3D Artist         | Beta 3.1 to Beta 4.0      |
| .sh4k3n          | 2D Artist         | Beta 1.0                  |
| Mikimus          | 3D Artist         | Beta 3.1 to Beta 4.0      |
| Hypergear        | Concept Artist    | Beta 3.1                  |
| HardHittinheeb   | Beta Tester       | Beta 3 to Beta 3.1        |
| PPK              | Beta Tester       | Beta 4 to 4.1             |
| Sp1nn3y          | Beta Tester       | Beta 4 to 4.1             |
| Yeyinde          | Beta Tester       | 4.1 to 4.2                |
