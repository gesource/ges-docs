---
title: "Content Creation"
weight: 600
draft: true
---

This section contains everything you need to know about making custom content for GoldenEye: Source! Whether you want to make levels or gameplay scenarios, we've got the knowledge you need.

{{% children  %}}

If you need help, post on our [Forums](https://forums.geshl2.com/index.php/board,134.0.html) or [Discord](https://geshl2.com/go/discord).
