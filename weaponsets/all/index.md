---
# This file was generated by the weaponsets-generator script: https://gitlab.com/gesource/ges-wiki-scripts
title: "All Weapon Sets"
description: "Shows a table of all weaponsets for quick reference."
---

{{% attachments sort="asc" /%}}

| Name | Print Name | Weight | Category |
|:-----|:-----------|:-------|---------:|
| km | [Killer Monkey's Favs](/weaponsets/custom_sets/km) | 50 | Custom_Sets |
| es | [Entropy-Set](/weaponsets/custom_sets/es) | 50 | Custom_Sets |
| luchador | [Luchador's Loadout](/weaponsets/custom_sets/luchador) | 50 | Custom_Sets |
| torn | [Torn's Noisy Boys](/weaponsets/custom_sets/torn) | 50 | Custom_Sets |
| tzkk | [TZKK's Shame](/weaponsets/custom_sets/tzkk) | 50 | Custom_Sets |
| mangley | [Mangley's Sneaky Set](/weaponsets/custom_sets/mangley) | 50 | Custom_Sets |
| pp7_arena | [PP7 Arena](/weaponsets/arenas/pp7_arena) | 0 | Arenas |
| pp7_silenced_arena | [PP7 Silenced Arena](/weaponsets/arenas/pp7_silenced_arena) | 0 | Arenas |
| dd44_arena | [DD44 Arena](/weaponsets/arenas/dd44_arena) | 0 | Arenas |
| silver_pp7_arena | [Silver PP7 Arena](/weaponsets/arenas/silver_pp7_arena) | 0 | Arenas |
| gold_pp7_arena | [Gold PP7 Arena](/weaponsets/arenas/gold_pp7_arena) | 0 | Arenas |
| magnum_arena | [Magnum Arena](/weaponsets/arenas/magnum_arena) | 0 | Arenas |
| golden_gun_arena | [Golden Gun Arena](/weaponsets/arenas/golden_gun_arena) | 0 | Arenas |
| shotgun_arena | [Shotgun Arena](/weaponsets/arenas/shotgun_arena) | 0 | Arenas |
| auto_shotgun_arena | [Auto Shotgun Arena](/weaponsets/arenas/auto_shotgun_arena) | 0 | Arenas |
| kf7_arena | [KF7 Arena](/weaponsets/arenas/kf7_arena) | 0 | Arenas |
| klobb_arena | [Klobb Arena](/weaponsets/arenas/klobb_arena) | 0 | Arenas |
| zmg_arena | [ZMG Arena](/weaponsets/arenas/zmg_arena) | 0 | Arenas |
| d5k_arena | [D5K Arena](/weaponsets/arenas/d5k_arena) | 0 | Arenas |
| d5k_silenced_arena | [D5K Silenced Arena](/weaponsets/arenas/d5k_silenced_arena) | 0 | Arenas |
| rcp90_arena | [RCP90 Arena](/weaponsets/arenas/rcp90_arena) | 0 | Arenas |
| ar33_arena | [AR33 Arena](/weaponsets/arenas/ar33_arena) | 0 | Arenas |
| phantom_arena | [Phantom Arena](/weaponsets/arenas/phantom_arena) | 0 | Arenas |
| sniper_rifle_arena | [Sniper Rifle Arena](/weaponsets/arenas/sniper_rifle_arena) | 0 | Arenas |
| throwing_knives | [Throwing Knives](/weaponsets/arenas/throwing_knives) | 0 | Arenas |
| throwing_knife_arena | [Throwing Knife Arena](/weaponsets/arenas/throwing_knife_arena) | 0 | Arenas |
| timed_mine_arena | [Timed Mine Arena](/weaponsets/arenas/timed_mine_arena) | 0 | Arenas |
| remote_mine_arena | [Remote Mine Arena](/weaponsets/arenas/remote_mine_arena) | 0 | Arenas |
| proximity_mine_arena | [Proximity Mine Arena](/weaponsets/arenas/proximity_mine_arena) | 0 | Arenas |
| grenade_arena | [Grenade Arena](/weaponsets/arenas/grenade_arena) | 0 | Arenas |
| grenade_launcher_arena | [Grenade Launcher Arena](/weaponsets/arenas/grenade_launcher_arena) | 0 | Arenas |
| rocket_launcher_arena | [Rocket Launcher Arena](/weaponsets/arenas/rocket_launcher_arena) | 0 | Arenas |
| rocket_arena | [Rocket Arena](/weaponsets/arenas/rocket_arena) | 0 | Arenas |
| moonraker_arena | [Moonraker Arena](/weaponsets/arenas/moonraker_arena) | 0 | Arenas |
| knife_arena | [Knife Arena](/weaponsets/arenas/knife_arena) | 0 | Arenas |
| ar_classic | [Classic List](/weaponsets/arsenal_mhide/ar_classic) | 0 | Arsenal_mhide |
| ar_action | [Action List](/weaponsets/arsenal_mhide/ar_action) | 0 | Arsenal_mhide |
| ar_skill | [Skill List](/weaponsets/arsenal_mhide/ar_skill) | 0 | Arsenal_mhide |
| ar_dinky | [Dinky List](/weaponsets/arsenal_mhide/ar_dinky) | 0 | Arsenal_mhide |
| ar_chaotic | [Chaotic List](/weaponsets/arsenal_mhide/ar_chaotic) | 0 | Arsenal_mhide |
| ar_mountain | [Mountain List](/weaponsets/arsenal_mhide/ar_mountain) | 0 | Arsenal_mhide |
| ar_woodandsteel | [Wood and Steel List](/weaponsets/arsenal_mhide/ar_woodandsteel) | 0 | Arsenal_mhide |
| ctf_power_weapons | [Power Weapons](/weaponsets/ctf_mhide/ctf_power_weapons) |  | CTF_mhide |
| ctf_lasers | [Lasers](/weaponsets/ctf_mhide/ctf_lasers) |  | CTF_mhide |
| ctf_random | [Random Weapon Set](/weaponsets/ctf_mhide/ctf_random) |  | CTF_mhide |
| caverns | [Caverns](/weaponsets/themed_mhide/caverns) | 0 | Themed_mhide |
| dam | [Byelomorye Dam](/weaponsets/themed_mhide/dam) | 0 | Themed_mhide |
| depot | [Depot](/weaponsets/themed_mhide/depot) | 0 | Themed_mhide |
| notused | [Weaponsets Not Used](/weaponsets/utility_mhide/notused) | 0 | Utility_mhide |
| test | [Weapon Spawn Test](/weaponsets/utility_mhide/test) | 0 | Utility_mhide |
| slappers | [Slappers Only](/weaponsets/default_sets/slappers) | 0 | Default_Sets |
| pistols_classic | [Pistols Classic](/weaponsets/default_sets/pistols_classic) | 500 | Default_Sets |
| sniper_rifles | [Sniper Rifles](/weaponsets/default_sets/sniper_rifles) | 300 | Default_Sets |
| automatics | [Automatics](/weaponsets/default_sets/automatics) | 400 | Default_Sets |
| assorted_weapons | [Assorted Weapons](/weaponsets/default_sets/assorted_weapons) | 400 | Default_Sets |
| mixed_weapons | [Mixed Weapons](/weaponsets/default_sets/mixed_weapons) | 400 | Default_Sets |
| grenades | [Grenades](/weaponsets/default_sets/grenades) | 500 | Default_Sets |
| timed_mines | [Timed Mines](/weaponsets/default_sets/timed_mines) | 500 | Default_Sets |
| light_explosives | [Light Explosives](/weaponsets/default_sets/light_explosives) | 400 | Default_Sets |
| pistols | [Pistols](/weaponsets/default_sets/pistols) | 500 | Default_Sets |
| rifles | [Rifles](/weaponsets/default_sets/rifles) | 400 | Default_Sets |
| full_automatics | [Full Automatics](/weaponsets/default_sets/full_automatics) | 400 | Default_Sets |
| lasers | [Lasers](/weaponsets/default_sets/lasers) | 500 | Default_Sets |
| shotguns | [Shotguns](/weaponsets/default_sets/shotguns) | 500 | Default_Sets |
| assault_weapons | [Assault Weapons](/weaponsets/default_sets/assault_weapons) | 500 | Default_Sets |
| light_scatter | [Light Scatter](/weaponsets/default_sets/light_scatter) | 500 | Default_Sets |
| combined_arms | [Combined Arms](/weaponsets/default_sets/combined_arms) | 500 | Default_Sets |
| jungle_weapons | [Jungle Weapons](/weaponsets/default_sets/jungle_weapons) | 250 | Default_Sets |
| power_weapons | [Power Weapons](/weaponsets/default_sets/power_weapons) | 500 | Default_Sets |
| golden_guns | [Golden Guns](/weaponsets/default_sets/golden_guns) | 0 | Default_Sets |
| heavy_weapons | [Heavy Weapons](/weaponsets/default_sets/heavy_weapons) | 400 | Default_Sets |
| grenade_launchers | [Grenade Launchers](/weaponsets/default_sets/grenade_launchers) | 500 | Default_Sets |
| rockets | [Rockets](/weaponsets/default_sets/rockets) | 500 | Default_Sets |
| remote_mines | [Remote Mines](/weaponsets/default_sets/remote_mines) | 300 | Default_Sets |
| proximity_mines | [Proximity Mines](/weaponsets/default_sets/proximity_mines) | 100 | Default_Sets |
| explosives | [Explosives](/weaponsets/default_sets/explosives) | 500 | Default_Sets |
| moonraker | [Moonraker](/weaponsets/default_sets/moonraker) | 400 | Default_Sets |
| silver_shooters | [Silver Shooters](/weaponsets/default_sets/silver_shooters) | 400 | Default_Sets |
| random | [Random Weapon Set](/weaponsets/default_sets/random) | 250 | Default_Sets |
