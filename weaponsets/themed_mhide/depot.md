---
# This file was generated by the weaponsets-generator script: https://gitlab.com/gesource/ges-wiki-scripts
title: "Depot"
---

{{<weaponset-metadata
  category="Themed_mhide"
  name="depot"
  weight="0"
>}}

{{<table-fit-content>}}
|    | Weapon      | Icon             |
|:--:|:------------|:-----------------|
| 1. | [PP7](/weapons/pp7) | ![](/weapons/images/weaponset-icons/pp7.png?lightbox=false) |
| 2. | [PP7 (Silenced)](/weapons/pp7_silenced) | ![](/weapons/images/weaponset-icons/pp7_silenced.png?lightbox=false) |
| 3. | [D5K Deutsche](/weapons/d5k_deutsche) | ![](/weapons/images/weaponset-icons/d5k.png?lightbox=false) |
| 4. | [D5K Deutsche](/weapons/d5k_deutsche) | ![](/weapons/images/weaponset-icons/d5k.png?lightbox=false) |
| 5. | [Phantom](/weapons/phantom) | ![](/weapons/images/weaponset-icons/phantom.png?lightbox=false) |
| 6. | [AR33](/weapons/ar33) | ![](/weapons/images/weaponset-icons/ar33.png?lightbox=false) |
| 7. | [Rocket Launcher](/weapons/rocket_launcher) | ![](/weapons/images/weaponset-icons/rocket_launcher.png?lightbox=false) |
| 8. | [Grenade Launcher](/weapons/grenade_launcher) | ![](/weapons/images/weaponset-icons/grenade_launcher.png?lightbox=false) |
