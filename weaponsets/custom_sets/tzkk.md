---
# This file was generated by the weaponsets-generator script: https://gitlab.com/gesource/ges-wiki-scripts
title: "TZKK's Shame"
---

{{<weaponset-metadata
  category="Custom_Sets"
  name="tzkk"
  weight="50"
>}}

{{<table-fit-content>}}
|    | Weapon      | Icon             |
|:--:|:------------|:-----------------|
| 1. | [ZMG](/weapons/zmg) | ![](/weapons/images/weaponset-icons/zmg.png?lightbox=false) |
| 2. | [Shotgun](/weapons/shotgun) | ![](/weapons/images/weaponset-icons/shotgun.png?lightbox=false) |
| 3. | [Cougar Magnum](/weapons/cougar_magnum) | ![](/weapons/images/weaponset-icons/cmag.png?lightbox=false) |
| 4. | [KF7 Soviet](/weapons/kf7_soviet) | ![](/weapons/images/weaponset-icons/kf7.png?lightbox=false) |
| 5. | [RC-P90](/weapons/rc-p90) | ![](/weapons/images/weaponset-icons/rcp90.png?lightbox=false) |
| 6. | [AR33](/weapons/ar33) | ![](/weapons/images/weaponset-icons/ar33.png?lightbox=false) |
| 7. | [Remote Mine](/weapons/remote_mine) | ![](/weapons/images/weaponset-icons/remotemine.png?lightbox=false) |
| 8. | [Automatic Shotgun](/weapons/automatic_shotgun) | ![](/weapons/images/weaponset-icons/auto_shotgun.png?lightbox=false) |
