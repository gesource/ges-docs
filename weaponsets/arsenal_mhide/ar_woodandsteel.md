---
# This file was generated by the weaponsets-generator script: https://gitlab.com/gesource/ges-wiki-scripts
title: "Wood and Steel List"
---

{{<weaponset-metadata
  category="Arsenal_mhide"
  name="ar_woodandsteel"
  weight="0"
>}}

{{<table-fit-content>}}
|    | Weapon      | Icon             |
|:--:|:------------|:-----------------|
| 1. | [RC-P90](/weapons/rc-p90) | ![](/weapons/images/weaponset-icons/rcp90.png?lightbox=false) |
| 2. | [Grenade Launcher](/weapons/grenade_launcher) | ![](/weapons/images/weaponset-icons/grenade_launcher.png?lightbox=false) |
| 3. | [Automatic Shotgun](/weapons/automatic_shotgun) | ![](/weapons/images/weaponset-icons/auto_shotgun.png?lightbox=false) |
| 4. | [Cougar Magnum](/weapons/cougar_magnum) | ![](/weapons/images/weaponset-icons/cmag.png?lightbox=false) |
| 5. | [Shotgun](/weapons/shotgun) | ![](/weapons/images/weaponset-icons/shotgun.png?lightbox=false) |
| 6. | [KF7 Soviet](/weapons/kf7_soviet) | ![](/weapons/images/weaponset-icons/kf7.png?lightbox=false) |
| 7. | [Throwing Knife](/weapons/throwing_knife) | ![](/weapons/images/weaponset-icons/knife_throwing.png?lightbox=false) |
| 8. | [Klobb](/weapons/klobb) | ![](/weapons/images/weaponset-icons/klobb.png?lightbox=false) |
