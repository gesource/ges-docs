---
title: "Weapon Sets"
weight: 100
---

Weapon sets, also known as loadouts, are the list of weapons which will spawn during the current round. By default, a different weapon set is randomly chosen with each new round.

GoldenEye: Source includes all weapon loadouts from GoldenEye 007 that can be formed with the weapons available. These loadouts have been augmented, often to include weapons previously unavailable and of appropriate power level. 

{{% children  depth="1" description="true" %}}