---
title: "Installation"
weight: 20
---

This article walks you through the steps of installing GoldenEye: Source on your PC.

## Install Steam

If you don't already have Steam installed, download it [here](https://steampowered.com/about). If you don't have an account, signing up is easy, and you don't have to make any purchases to play GoldenEye: Source.

Also necessary is Source SDK Base 2007. Steam will install this for you when launching GE:S, but it can also be found by searching your Steam library or by clicking [here](steam://install/218) \*.

*\* This link requires Steam to already be installed.*

## Install GoldenEye: Source

1. Download GoldenEye: Source here: [Download](https://geshl2.com/go/download).
1. Run the downloaded file. The downloaded program will install GE:S for you.

## Restart Steam

For GoldenEye: Source to appear in your games library, Steam must be restarted after the installation is completed.

## Next Steps

**You're done!** If you encounter any issues, check our [Troubleshooting](../troubleshooting) page.
