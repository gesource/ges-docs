---
title: "System Requirements"
weight: 10
---

To play GoldenEye: Source, your PC needs to meet the following requirements:

### Minimum

**Operating System**: Windows 10 or newer \
**Processor**: Intel, AMD \
**Graphics**: DirectX 9 support required \
**System Memory (RAM)**: 4GB \
**Hard Drive**: At least 10GB free space

### Recommended

**Operating System**: Windows 10 or newer \
**Processor**: Intel Core, AMD Ryzen \
**Graphics**: DirectX 9 support required; dedicated graphics card recommended \
**System Memory (RAM)**: 8GB \
**Hard Drive**: At least 10GB free space

These requirements are pretty lightweight by modern standards, and most PCs will run GoldenEye: Source well. Older systems (or systems with weaker integrated graphics) may need to lower the graphical details in order to get smooth performance.

## Next Steps

* [Install GoldenEye: Source](../install)
