---
title: "Manual Installation"
weight: 21
---

This article details how to manually install GoldenEye: Source. This can be useful for certain scenarios, such as:

* You don't want to run the installer
* You want to install GE:S to a non-standard location (i.e. somewhere outside of your normal Steam library location)

This article is for advanced users. We recommend most users visit the regular [Installation guide](../install) instead.

## Install Steam

Steam must be installed and the user must have a Steam account. Download Steam [here](https://steampowered.com/about).

## Install Prerequisite

In order to play GoldenEye: Source v5.0, you need the [Visual C++ 2012 Redistributable (x86)](https://www.microsoft.com/en-gb/download/details.aspx?id=30679). 

{{% notice style="info" %}}
Make sure you choose `vcredist_x86.exe`. **The 64-bit version will not work!**
{{% /notice %}}

## Download GoldenEye: Source

Download the 7-Zip archive of GE:S from the [alternative downloads](https://www.geshl2.com/alternative-downloads/) page.

{{% notice style="info" %}}
You will need a compatible archival program such as [7-Zip](https://7-zip.org/) in order to extract this file.
{{% /notice %}}

## Install GoldenEye: Source

The below instructions tell you how to install GE:S. They vary slightly depending on whether you want to install it to your primary Steam Library location or somewhere else, so choose the appropriate tab depending on what you want to do.

{{< tabs title="Choose Install Type:" >}}

{{% tab title="Standard Install Path (Most Players)" %}}
Easy peasy! Just extract the .7z file to `Steam\SteamApps\sourcemods\gesource`.

The Steam folder is typically located in `C:\Program Files (x86)`. You can only install to the library where Steam itself is installed. If you want to install GE:S somewhere else, choose the **Custom Install Path** tab.

{{% /tab %}}

{{% tab title="Custom Install Path" %}}
Use these steps if you want to install GE:S to a drive other than the one Steam is installed on.

1. Extract the .7z file to your desired install location
1. Open a Command Prompt or PowerShell as Administrator
1. Run the following command:\
    `mklink /D "D:\path\to\gesource" "C:\Program Files (x86)\Steam\SteamApps\sourcemods\gesource"`\
    \
    *Replace `D:\path\to\gesource` with the path of your `gesource` folder*\
    *Replace `C:\Program Files (x86)\Steam` with the path of your Steam folder*
    
Steam will only detect sourcemods from the primary Steam Library location. The above command creates a directory symbolic link that allows Steam to read the gesource folder as if it was actually in your primary Steam library.
{{% /tab %}}

{{< /tabs >}}

## Restart Steam

For GoldenEye: Source to appear in your games library, Steam must be restarted after the installation is completed.


