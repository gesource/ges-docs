---
title: "Console Command Reference"
weight: 990
---

This is a list of console commands for GoldenEye: Source. These can be useful for power users, content creators, server admins, and programmers.

---

`int` parameters are input as whole number values such as 28

`float` parameters are input as floating point values such as 3.98

`bool` parameters are input as either 1 or 0, 1 being True and 0 being False

`string` parameters are input as arbitrary unicode strings, such as “ge_archives”

## Gameplay Commands

|Command|Input|Description|
|:------|:----|:----------|
|changelevel|string|Ends the match and changes the level to whichever was named |
|mp_timelimit|int|Match (map) time limit, in minutes.{{<br>}}0 = no time limit|
|ge_roundtime|int|Round time, in seconds.{{<br>}}0 = no time limit.|
|ge_restartround||Ends the current round and starts the next one Use this to move to the next weapon set quickly|
|ge_gameplaylist||Prints the gameplays (game modes) on the server|
|ge_gameplaylistrefresh||Refreshes the list of gameplays. Useful if you added a gameplay while the server was running|
|ge_gameplay|string|Sets the current game mode|
|ge_gameplayreload||Reloads the current gameplay (FOR [DEVELOPERS!](https://www.youtube.com/watch?v=rRm0NDo1CiY))|
|ge_weaponset_list||Prints the weaponsets on the server|
|ge_weaponset_reload||Refreshes the list of weaponsets. Useful if you added a weaponset while the server was running|
|ge_weaponset|string|Sets the weaponset (takes effect next round)|
|ge_startarmed|bool|Sets if players start with the first weapon in the weaponset{{<br>}}0 = Slapper{{<br>}}1 = Slapper + Level 1 weapon|
|ge_weapondroplimit|int|The max number of weapons dropped on death|

## Round/Match Time

|Command|Input|Description|
|:------|:----|:----------|
|changelevel|string|Ends the match and changes the level to whichever was named |
|mp_timelimit|int|Match (map) time limit, in minutes.{{<br>}}0 = no time limit|
|ge_roundtime|int|Round time, in seconds.{{<br>}}0 = no time limit.|
|ge_rounddelay|int|Delay in seconds between rounds|
|ge_roundcount|int|Number of rounds in the current match.{{<br>}}Calculates ge_roundtime{{<br>}}Use 0 to disable|
|ge_restartround||Ends the current round and starts the next one Use this to move to the next weapon set quickly|
|ge_endround||Ends the current round. Use `ge_endround 0` to skip scores.|
|ge_endround_keepweapons||Ends the current round but keeps the same weaponset|
|ge_endmatch||Ends the match, loading the next one|
|ge_setcurrentroundtime|int|Sets the number of seconds left in the current round|
|ge_addtoroundtime|int|Adds the given number of seconds to the current roundtime|

## Mapchooser

|Command|Input|Description|
|:------|:----|:----------|
|ge_loadouts_loadoutbuffercount|int|How often weaponsets are allowed to repeat without weight penalty. Any that have been played within (this value)/2 rounds cannot be picked randomly.|
|ge_mapchooser_avoidteamplay|bool|If set to 1, server will avoid choosing maps where the current playercount is above the team threshold.|
|ge_mapchooser_mapbuffercount|int|How many other maps need to be played since the last time a map was played before it can be selected randomly again|
|ge_mapchooser_usemapcycle|bool|If set to 1, the server will just use the mapcycle file (disables selection based on map scripts)|
|ge_mapchooser_resthreshold|int|The mapchooser will do everything it can to avoid switcing between maps with this combined resintensity.|
|ge_print_map_selection_data||Prints the server's map selection data|
ge_print_current_map_data||Prints the current map's data|
|ge_print_map_selection_weights||Prints the map selection chance for given playercount, or current playercount if none is given. Use 1 as second parameter for unsorted list|
|ge_gameplay_threshold|int|Playercount must be exceeded before gamemodes other than Deathmatch will be randomly chosen|
|ge_gameplay_modebufferpentaly|int|How much to take off of the weight of a mode for each time it appears in the buffer|
|ge_gameplay_mode|int|Mode to choose for next gameplay{{<br>}}0 = Same as last map{{<br>}}1 = Random from current map{{<br>}}2 = Use gameplay cycle file|
|ge_gp_cyclefile|string|The gameplay cycle to use for the random gameplay or ordered gameplay.{{<br>}}If ge_gameplay_mode is not 1, this has no effect.|

## Teamplay

|Command|Input|Description|
|:------|:----|:----------|
|ge_teamplay|bool|Turns on teamplay if the current gameplay scenario supports it|
|ge_autoteam|int|Automatically enables teamplay if the playercount is above this value{{<br>}}Disables teamplay if the playercount falls below this value|
|ge_autoautoteam|int|If set to 1, the server will set ge_autoteam to the value specified in the current map script file|
|ge_teamautobalance|bool|turns on the auto balancer for teamplay|

## Gameplay Tweaks

|Command|Input|Description|
|:------|:----|:----------|
|ge_partialammopickups|bool|Allow players to pick up ammo from  crate without picking up all of it|
|ge_armorrespawntime|int|Minimum time in seconds before armor respawns|
|ge_armorrespawn_pc_scale|float|Multiplier applied to playercount|
|ge_armorrespawn_pc_power|float|Power applied to playercount|
|ge_respawndelay|int|Changes the minimum delay between respawns|
|ge_itemrespawntime|int|Time in seconds between ammo respawns|
|ge_weaponrespawntime|int|Time in seconds between weapon spawns (ge_dynamicweaponrespawn must be off!)|
|ge_dynamicweaponrespawn|bool|Changes the respawn delay for weapons and ammo to be based on how many players are connected|
|ge_dynamicweaponrespawn_scale|float|Changes the dynamic respawn delay scale for weapons and ammo|
|ge_allowjump|bool|Toggle allowing players to jump|
|ge_exp_allowz|bool|Allow excessive Z forces on explosions|
|ge_tournamentmode|bool|Turns on tournament mode that disables certain gameplay checks|
|ge_allowradar|bool|Allow clients to use their radars|
|ge_radar_range|int|Change the radar range (in inches)|
|ge_radar_showenemyteam|bool|Allow the radar to show enemies during teamplay (useful for tournaments)|

## Fun

|Command|Input|Description|
|:------|:----|:----------|
|ge_weapondroplimit|int|Cap on the number of weapons a player can drop on death in addition to their active one|
|ge_exp_pushscale|int|Scale the amount of push force from explosions on players|
|ge_explosiveheadshots|bool|Headshot kills create explosions|
|ge_velocity|float|Player movement velocity multiplier. Applies in multiples of 0.5 \[0.5 to 2.0\]|
|ge_paintball|bool|Enables paintball bullet decals|
|ge_infiniteammo|bool|Enables infinite ammo mode|

## Bots

|Command|Input|Description|
|:------|:----|:----------|
|ge_bot_threshold|int|Server will maintain this many players using bots. If more players join, the extra bots will disconnect.|
|ge_bot_openslots|int|Number of open slots to leave for incoming players|
|ge_bot_strict_openslot|int|Count spectators in determining whether to leave an open player slot|
|ge_bot||Adds a bot|
|ge_bot_remove|int|Removes number of bots. If no number is supplied, it removes them all|
|ge_bot_givespawninvuln|bool|Bots are allowed to have spawn invulnerability|
ge_bot_difficulty|int|Sets average difficulty of the AI \[0-9\]|
