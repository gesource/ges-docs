---
title: "Troubleshooting"
weight: 999
---

This article will teach you how to resolve the most common issues our community has encountered.

Check the Table of Contents (the blue button in the top-left of this page) for your issue. Click on the issue to jump to it.

## Installer

### "NSIS error", "Non 7z archive", Or "7-Zip: CRC Error"

The most likely cause is that the installer program was corrupted. [Download](https://www.geshl2.com/go/download) a new copy.

Still Having Issues with the Installer? Try installing GE:S [manually](../manual-install).

## Game

### No Text In Menus

This issue is typically observed when running the game at very high resolutions, e.g. 4K. The cause is a vertical resolution higher than 1800 pixels.

{{% expand title="Resolution" %}}

To change the display resolution when the game menus do not display properly, follow these steps:

1. Go to your Steam library
1. Right-click GoldenEye: Source (vX.X)
1. Click Properties
1. Under LAUNCH OPTIONS, enter the following, replacing 1920 and 1080 with your screen's width and height as required. **Do not exceed a vertical resolution (-h) of 1800**.
1. -w 1920 -h 1080
1. Close the properties window
1. Launch the game to see if the issue is resolved.

You may at this point remove the resolution setting from your launch options, or it will ignore your in-game setting and reset the resolution at every launch.

{{% /expand %}}

### Game Crashes on Launch

Install the 32-bit (x86) version of the [Visual Studio C++ 2012 redistributable]().

Make sure you choose `vcredist_x86.exe`. **The 64-bit version will not work!**

### Game Crashes During Gameplay

* In Options > Video > Advanced, try setting your Texture Detail to Medium or lower.
* Try disabling any game overlays. For example, the Steam overlay, Discord overlay, and Xbox Game Bar.

### Volume Is Reduced When Joining a Server

This is a Windows feature which reduces the volume of other applications when communication activity is detected. This is triggered because of a standard Source engine behavior which activates your microphone so it is ready when you press your push-to-talk key.

{{% expand title="Resolution" %}}

1. Press `Windows + R`
1. In the Run box, type `control mmsys.cpl` sounds
   ![](images/windows-audio-ducking/audioducking-pic-1.png)
1. Click OK
1. Click the Communications tab
   ![](images/windows-audio-ducking/audioducking-pic-2.png)
1. Click Do nothing
   ![](images/windows-audio-ducking/audioducking-pic-3.png)
1. Click OK.
   ![](images/windows-audio-ducking/audioducking-pic-4.png)

Your background audio should no longer be reduced when joining a server.

{{% /expand %}}

### "Failed to load library client"

This indicates that the .dll files for GoldenEye: Source are corrupt or missing.

{{% expand title="Resolution" %}}

* Download and reinstall GoldenEye: Source.
* Check that you aren't running out of disk space.
    As of v5.0, the installer does not check for disk space and will fail silently when extracting files. This will be fixed for the next major release.
* If you're running low on disk space, check the steps on [Manual Install](../manual-install) which show you how to install on a second hard drive.

{{% /expand %}}

### "Failed to start game (missing executable)"

Validate your local files for Source SDK Base 2007.

{{% expand title="Resolution" %}}

1. Open the Library tab of Steam
1. In the search box, type “Source SDK Base 2007”
1. Right-click Source SDK Base 2007
1. Click Properties...
1. Click Installed Files
1. Click Verify integrity of tool files
    
{{% /expand %}}

### VSync Is Forced On

Disable full-screen optimizations for hl2.exe

{{% expand title="Resolution" %}}

1. Open the Library tab of Steam
1. In the search box, type “Source SDK Base 2007”
1. Right-click Source SDK Base 2007
1. Go to Manage > Browse local files \
*A File Explorer window will appear.*
1. Right-click hl2.exe
1. Click Properties
1. Click the Compatibility tab
1. Check "Disable fullscreen optimizations"
1. Click OK to save the changes

**Note:** If you do this, you may notice strange frame pacing issues. To resolve this, set `fps_max` to a multiple of your refresh rate. For example:

* 60Hz: set `fps_max` to 120
* 75Hz: set `fps_max` to 150
* 120Hz or higher: set `fps_max` to your refresh rate (120Hz monitor = `fps_max 120`).

To set fps_max permanently, you can create or modify the file `gesource/cfg/autoexec.cfg` and add `fps_max ##` on its own line.
    
{{% /expand %}}

### Stuttering/Inconsistent Frame Rate

#### Solution 1: CPU Affinity

You might experience stuttering when GE:S is using 100% of the CPU core it currently occupies. You may also observe that the load is shifting between different CPU cores (we believe this is what causes the stutter).

See the below instructions for how to pin the game to a specific CPU core.

{{% expand title="Resolution" %}}

* Go to your Steam library
* Right-click GoldenEye: Source (vX.X)
* Click Properties
* Under LAUNCH OPTIONS, enter the following command:
* `C:\Windows\System32\cmd.exe /S /C start /AFFINITY 4 "." %command%`
* Close the properties window
* Launch the game to see if the issue is resolved.

Notes:

* Source engine launch options can be added after `%command%`
* This example command will launch the game on the 3rd logical core. The affinity is hexadecimal
* On systems with HyperThreading (HT) or Simultaneous Multi-Threading (SMT), where the system has 2 logical threads per core, make sure you are pinning it to a physical core
* If you want to change the affinity, see this handy [Affinity Mask Calculator](https://bitsum.com/tools/cpu-affinity-calculator/)

{{% /expand %}}

#### Solution 2: Discord Hardware Accelleration

If you use Discord, it has been reported that disabling hardware acceleration within the app can resolve the issue.

{{% expand title="Resolution" %}}

1. In Discord, go to User Settings (the cog next to your user info and voice controls)
1. Go to Advanced
1. Turn off Hardware Acceleration

{{% /expand %}}

#### Solution 3: Nvidia Control Panel - Max Frame Rate

If you have an Nvidia GPU, you can set a Max Frame Rate for the hl2.exe executable.

{{% expand title="Resolution" %}}

1. Open the **Nvidia Control Panel**
1. Click **Manage 3D settings**
1. Click on the **Program Settings** tab
1. From the **Select a program to customize:** drop down, look for **Half Life: Source (hl2.exe)**. If you can't find it, click the **Add** button. It might show up as a recently used program, if you've recently launched the game. If not, click the Browse button, browse to `C:\Program Files (x86)\Steam\steamapps\common\Source SDK Base 2007` and select `hl2.exe`
1. Under **Specify the settings for this program:**, scroll down until you find the **Max Frame Rate** Feature, change Setting to **120 FPS** (or some other multiple of 60, but most have reported that 120 works the best)
![](images/nvidia-control-panel.png)
1. If GoldenEye: Source is currently running, quit and restart, see if the issue is resolved

{{% /expand %}}

### When All Else Fails

* Use your search engine of choice to look up the issue. Rather than mentioning GoldenEye: Source specifically, search for “Source engine” or other Source engine games such as “HL2”, “Gmod”, or “TF2”.
* Make sure your system is up to date. This includes Windows updates, but especially includes graphics drivers.
* Some users have reported issues that were resolved by uninstalling and reinstalling their graphics drivers.
* Use a utility like Display Driver Uninstaller (DDU) and then download and reinstall the latest driver directly off the manufacturer's website.
* And finally, try [completely removing](../manual-uninstall) and reinstalling GoldenEye: Source and Source SDK Base 2007.
