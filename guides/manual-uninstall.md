---
title: "Manual Uninstall"
weight: 22
---

If you want to remove GoldenEye: Source or you're reinstalling to resolve an issue, this article will show you how to remove every trace of it from your computer. 

{{% notice style="note" %}}
This article is intended for advanced users. Unless you have a reason to follow this guide, we recommend you simply uninstall GoldenEye: Source from Windows Settings > Apps.

Please follow this guide carefully or you may have undesirable results. 
{{% /notice %}}

## Back Up Your Data

These files are at the root of your `gesource` folder. The `gesource` folder is located inside `SteamDir\steamapps\sourcemods`, where `SteamDir` is the location where Steam is installed.

The following files (if they exist) contain your achievements. You may want to back these up, or you will need to start from scratch when playing GoldenEye: Source again:

* `gamestate.txt`
* `gesdata.txt`

The rest of your configuration data consists of system settings and preferences:

* System settings consist of things like display resolution and are stored in the registry (see below)
* Preferences consist of things like key bindings, mouse sensitivity, and other game settings.
  These settings are in `cfg\config.cfg`, but you may want to also back up the rest of your cfg folder if you manually modified anything else.
* **If you're uninstalling to resolve an issue, first test a *clean* installation *without* any modifications to check if a .cfg file is causing the issue.**

## Remove Game Files

In the `steamapps\sourcemods` folder, delete the following files and folders:

* `gesource (directory)`
* `gesource_uninstall.exe`
* `gesource_run.exe`

## Remove Registry Keys

{{% notice style="warning" %}}
Be careful and make sure you do not delete the wrong registry key. Doing so could damage other programs or make your system inoperable.
{{% /notice %}}

Press `Windows + R`, type `regedit` in the Run box, and press Enter.

### System Settings

Registry key that contains system settings for GoldenEye: Source

In the Registry Editor, navigate to `HKEY_CURRENT_USER\Software\Valve\Source`. Below this key will be a number of subkeys that correspond to the directory in which GoldenEye: Source was installed. For example, many users will have a registry key location similar to `HKEY_CURRENT_USER\Software\Valve\Source\C:\Program Files (x86)\Steam\steamapps\sourcemods\gesource`. Delete the `gesource` key.

### Uninstall Data

Registry key that contains information about the program. Windows displays this information in the Settings app and Control Panel.

1. On 32-bit systems, navigate to `HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Uninstall`
1. Delete the `gesource` key.
#
1. On 64-bit systems, navigate to `HKEY_LOCAL_MACHINE\Software\Wow6432node\Microsoft\Windows\CurrentVersion\Uninstall`
1. Delete the `gesource` key.

## Source SDK Base 2007

If you don't have any other Source modifications installed, then it is safe to remove Source SDK Base 2007. If you're uninstalling to resolve issues, you may want to also remove and reinstall Source SDK Base 2007. Some users have reported issues that were resolved by reinstalling or validating it.

In the Steam library, navigate to the Tools tab. Right-click Source SDK Base 2007 and click Uninstall. When asked to confirm the uninstallation, click Delete.

Navigate to the Steam library folder where Source SDK Base 2007 was installed. Go to the `common` folder and delete the `Source SDK Base 2007` folder if it exists.

