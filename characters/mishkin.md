---
title: "Mishkin"
weight: 80
---

{{<character-info
	name="Dimitri Mishkin"
	occupation="Defense Minister"
	affiliation="Russian Federation"
	height=""
	nationality="Russian"
	status="Deceased"
>}}

Dimitri Mishkin is a playable character in GoldenEye: Source, and was featured in the 1995 *GoldenEye* film and 1997 *GoldenEye 007* video game. Mishkin was the Russian Minister of Defense, who initially suspected James Bond and MI6 were responsible for the Severnaya disaster. 
