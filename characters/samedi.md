---
title: "Samedi"
weight: 110
---

{{<character-info
	name="Baron Samedi"
	aliases="\"The man who cannot die\""
	occupation="Voodoo God of Death"
	affiliation="Dr. Kananga, Voodoo occult "
	height="6'6\""
	nationality="Unknown"
	status="Unknown"
>}}

Baron Samedi is a playable character in GoldenEye: Source, and based on the character featured in the 1973 film *Live and Let Die* and the 1997 *GoldenEye 007* video game. Baron Samedi is a mysterious villain, possibly with supernatural powers. Is he a man imitating the voodoo god, or the real thing? 
