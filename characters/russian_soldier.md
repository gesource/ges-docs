---
title: "Russian Soldier"
weight: 50
---

{{<character-info
	name="Unknown"
	occupation="Soldier"
	affiliation="Janus Syndicate; Russian Federation"
	height=""
	nationality="Russian"
	status="Active"
>}}

The Russian Soldier is a playable character in GoldenEye: Source, and based off the Russian Soldier enemies from in the 1997 *GoldenEye 007* video game. 

## In GoldenEye: Source

* The Russian Soldier is one of two new playable characters introduced in version 5.0; the other being the [Russian Infantry](russian_infantry)
* The Russian Soldier has 4 different hat/head combinations available to choose from:
  * Metal Helmet
  * Green Beret
  * Black Beret
  * No Hat
* Their faces are modeled after former developers Dr Dean and Jonathan SSL

## In GoldenEye: 007 (N64)

* In the game and the movie, Russian Soldiers were the most frequently-seen hostile characters.
* Russian Soldiers appeared in Dam, Facility, Runway, Bunker, Archives and Streets.
