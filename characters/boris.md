---
title: "Boris"
weight: 40
---

{{<character-info
	name="Boris Grishenko"
	occupation="Computer Programmer"
	affiliation="Janus Syndicate"
	height="5'10\""
	nationality="Russian"
	status="Deceased"
>}}

Boris Grishenko is a playable character in GoldenEye: Source, and one of several villains featured in the 1995 *GoldenEye* film and 1997 *GoldenEye 007* video game. Boris worked at the Severnaya Observatory in Siberia which watched over the two GoldenEye satellites, Petya and Mischa.

## Trivia

* Boris appears only twice in the GoldenEye 007 video game.
* During the GoldenEye 007 campaign, If Boris is killed during his encounter in the satellite control center, Natalya gets angry with Bond and refuses to operate the computer. As a result, the mission is failed.
* Boris' trademark phrase is “I am invincible!”

