---
title: "Female Scientist"
weight: 30
---

{{<character-info
	name="Unknown"
	occupation="Scientist"
	affiliation="MI6"
	height=""
	nationality="French"
	status="Active"
>}}

The Female Scientist is a playable character in GoldenEye: Source. She was modeled after former developer Spider's girlfriend at the time.
