---
title: "Russian Infantry"
weight: 60
---

{{<character-info
	name="Unknown"
	occupation="Infantryman"
	affiliation="Janus Syndicate; Russian Federation"
	height=""
	nationality="Russian"
	status="Active"
>}}

The Russian Infantry is a playable character in GoldenEye: Source, and based off the Russian infantry enemies from in the 1997 *GoldenEye 007* video game. 

## In GoldenEye: Source

* The Russian infantry is one of two new playable characters introduced in version 5.0; the other being the [Russian Soldier](russian_soldier)
* The Russian Infantry has the option to remove his hat on the character selection screen
* The infantry's face is modeled after former Beta Tester, Cy

## In GoldenEye: 007 (N64)

* Russian Infantry appeared in Silo, Statue and Egyptian
