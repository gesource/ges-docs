---
title: "Jaws"
weight: 100
---

{{<character-info
	name="Unknown"
	aliases="Jaws"
	occupation="Henchman; Assassin"
	affiliation="Hugo Drax; Karl Stromberg"
	height="7'2\""
	nationality="Polish"
	status="Unknown"
>}}

Jaws is a playable character in GoldenEye: Source, and based on the character first featured in the 1977 film *The Spy Who Loved Me* and the 1997 *GoldenEye 007* video game. Standing at an imposing 7 feet tall, and sporting razor-sharp metal teeth, Jaws was a seemingly invincible henchman to the villain, Karl Stromberg. He would later appear in the 1979 sequel *Moonraker* as a henchman to the villain Hugo Drax. 
