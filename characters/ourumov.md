---
title: "Ouromov"
weight: 70
---

{{<character-info
	name="Arkady Grigorovich Ourumov"
	occupation="Commander, Russian Space Division"
	affiliation="Janus Syndicate; Russian Federation"
	height="6'2\""
	nationality="Russian"
	status="Deceased"
>}}

Arkady Ourumov is a playable character in GoldenEye: Source, and one of several villains featured in the 1995 *GoldenEye* film and 1997 *GoldenEye 007* video game. Formerly a Soviet Colonel in charge of a Chemical Weapons Facility, he went on to become a General and the head of the Russian Space Division.

## In GoldenEye: Source

Ourumov has 2 different player models available to choose from: His most recent model, as well as an older version that has come to be known in some circles as “80's Ourumov”. 
