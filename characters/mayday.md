---
title: "Mayday"
weight: 120
---

{{<character-info
	name="May Day"
	occupation="Bodyguard; Henchman"
	affiliation="Max Zorin"
	height=""
	nationality="American"
	status="Deceased"
>}}

Mayday is a playable character in GoldenEye: Source, and based on the character featured in the 1985 film *A View to a Kill* and the 1997 *GoldenEye 007* video game. Possessing superhuman strength in the film, Mayday is the bodyguard and head of an all-female group of guards for chief villain Max Zorin. 
