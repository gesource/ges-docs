---
title: "Oddjob"
weight: 130
---

{{<character-info
	name="Unknown"
	occupation="Bodyguard; Henchman"
	affiliation="Auric Goldfinger"
	height=""
	nationality="Korean"
	status="Deceased"
>}}

Oddjob is a playable character in GoldenEye: Source, and based on the character featured in the 1964 film *Goldfinger* and the 1997 *GoldenEye 007* video game. As the mysterious and silent bodyguard of Auric Goldfinger, Oddjob sports a bowler hat lined with a razor-sharp blade. This iconic henchman of Goldfinger easily manhandles Bond and seems unfazed by Bond's attempts at hand to hand combat. 
