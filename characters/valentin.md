---
title: "Valentin"
weight: 90
---

{{<character-info
	name="Valentin Zukovsky"
	occupation="KGB Agent (formerly); Russian Mafia Boss"
	affiliation="KGB (formerly); Russian Mafia "
	height="6'1\""
	nationality="Russian"
	status="Active"
>}}

Valentin Zukovsky is a playable character in GoldenEye: Source, and was featured in the 1995 *GoldenEye* film and 1997 *GoldenEye 007* video game. He was the feared head of the Russian Mafia in St. Petersburg, a former KGB agent, and was Bond's one time enemy.

## In GoldenEye 007 (N64)

* It is possible to run over Valentin with the tank after speaking with him during the Streets mission.
