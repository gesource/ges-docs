---
title: "Trevelyan"
weight: 20
---

{{<character-info
	name="Alec Trevelyan"
	aliases="006, Janus"
	occupation="00 Agent (formerly); Head of Janus Syndicate"
	affiliation="MI6 (formerly); Janus Syndicate"
	height="5'11\""
	nationality="British"
	status="Deceased"
>}}

Alec Trevelyan is a playable character in GoldenEye: Source, and the main antagonist in the 1995 *GoldenEye* film and 1997 *GoldenEye 007* video game. 

## In GoldenEye: Source

Trevelyan's outfit and appearance are based off the character's appearance prior to the the Chemical Weapons Facility explosion seen in the *GoldenEye* film and *GoldenEye 007* video game. 
