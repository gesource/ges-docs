---
title: "Bond"
weight: 10
---

{{<character-info
	name="James Bond"
	aliases="007"
	occupation="00 Agent, British Intelligence"
	affiliation="MI6"
	height="6'1\""
	nationality="British"
	status="Active"
>}}

James Bond is a fictional British Secret Service agent created in 1953 by writer Ian Fleming. He is the main protagonist in the James Bond series of novelizations, films and video games.

Bond is an agent of the international arm of the British Secret Service headquartered in London, and as an agent of the Secret Service, Bond holds code number “007.” The 'double-0' prefix indicates his discretionary licence to kill in the performance of his duties.

## In GoldenEye: Source

Bond has a total of 3 different suit colors to choose from:

* Black
* Blue
* Brown

