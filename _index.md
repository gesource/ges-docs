---
title: "Welcome"
disableToc: true
---

### Welcome to the GoldenEye: Source documentation website!

Our goal is to gather all the information you need to know about GE:S in a single place.

### Sections

{{% children depth=1 %}}

### External Links

* [Frontpage](https://www.geshl2.com/)
* [Download](https://www.geshl2.com/download/)
* [News](https://www.geshl2.com/posts/)

### Community

* [Forums](https://forums.geshl2.com)
* [Discord](https://discordapp.com/invite/MTwT3Ch)
